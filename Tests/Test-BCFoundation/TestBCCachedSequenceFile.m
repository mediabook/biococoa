//
//  TestBCCachedSequenceFile.m
//  BioCocoa
//
//  Created by Scott Christley on 9/25/07.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "TestBCCachedSequenceFile.h"


@implementation TestBCCachedSequenceFile

// read fasta format, DNA
- (void)testReadDNAFastaFile
{
  NSLog(@"-testReadDNAFastaFile");

  NSBundle *aBundle = [NSBundle bundleForClass: [self class]];
  NSString *fileName = [aBundle pathForResource: @"test" ofType: @"fa"];
	
  // read as cache and read in memory, then compare
  BCCachedSequenceFile *cacheFile = [BCCachedSequenceFile readCachedFileUsingPath: fileName];

  BCSequenceReader *sequenceReader = [[[BCSequenceReader alloc] init] autorelease];
  BCSequenceArray *sequenceArray = [sequenceReader readFileUsingPath: fileName format: BCFastaFileFormat];

	// errors are concatenated
	NSMutableString *error=[NSMutableString stringWithString:@""];
  
  if (cacheFile == nil)
    [error appendString: @"cache file is nil\n"];
  else {
    if (sequenceArray == nil)
      [error appendString: @"Sequence array is nil\n"];
    else {
      if ([sequenceArray count] != 2)
        [error appendFormat: @"Number of sequence in array is incorrect, 2 != %d\n", [sequenceArray count]];
      else {
        BCSequence *aSeq = [sequenceArray sequenceAtIndex: 0];
        BCSequence *revSeq = [aSeq reverseComplement];
        const unsigned char *seqData1 = [aSeq bytes];
        int aLen = [aSeq length];
        char seqData2[aLen];

        if (aLen != 517)
          [error appendFormat: @"Length of sequence is incorrect, 517 != %d\n", aLen];

        // forward strand
        int result = [cacheFile symbols: seqData2 atPosition: 0 ofLength: aLen forSequenceNumber: 0];
        if (result != aLen)
          [error appendFormat: @"Could not read full length of sequence, %d != %d\n", aLen, result];
        int i;
        for (i = 0; i < result; ++i) {
          if (seqData1[i] != seqData2[i]) {
            [error appendFormat: @"Sequence data does not match at position %d, %c != %c\n", i, seqData1[i], seqData2[i]];
            break;
          }
        }

#if 0 // reverse complement in BCSequence not working right
        NSLog(@"%@\n", [revSeq sequenceString]);
        // reverse strand
        seqData1 = [revSeq bytes];
        result = [cacheFile symbols: seqData2 atPosition: aLen ofLength: aLen forSequenceNumber: 0];
        for (i = 0; i < result; ++i) {
          if (seqData1[i] != seqData2[i]) {
            [error appendFormat: @"Sequence data does not match at position %d, %c != %c\n", i, seqData1[i], seqData2[i]];
            break;
          }
        }
#endif
      }
    }
  }

	// if error!=@"", the test failed
	STAssertTrue ([error isEqualToString:@""], error);
}

@end
