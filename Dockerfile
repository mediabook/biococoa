# GNUstep base image
FROM mediabook/gnustep

MAINTAINER Scott Christley <schristley@mac.com>

# compile and install BioCocoa
COPY . /mediabook/biococoa-src
RUN cd /mediabook/biococoa-src && make install
RUN /sbin/ldconfig
