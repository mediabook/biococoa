//
//  BCEntrezController.m
//
//  Created by Koen van der Drift on 9/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//
//
//  Based on EntrezController, that was written by Alexander Griekspoor 
//  for the BioCocoa Project
//
//  EntrezController.m
//  BioCocoa
//
//  Created by Alexander Griekspoor
//  Copyright (c) 2006 Mekentosj.com. All rights reserved.
//  http://creativecommons.org/licenses/by-nc/2.0/
//
//  Permission to use, copy, modify and distribute this software and its documentation
//  is hereby granted, provided that both the copyright notice and this permission
//  notice appear in all copies of the software, derivative works or modified versions,
//  and any portions thereof, and that both notices appear in supporting documentation,
//  and that credit is given to Mekentosj.com in all documents and publicity
//  pertaining to direct or indirect use of this code or its derivatives.
//
//  THIS IS EXPERIMENTAL SOFTWARE AND IT IS KNOWN TO HAVE BUGS, SOME OF WHICH MAY HAVE
//  SERIOUS CONSEQUENCES. THE COPYRIGHT HOLDER ALLOWS FREE USE OF THIS SOFTWARE IN ITS
//  "AS IS" CONDITION. THE COPYRIGHT HOLDER DISCLAIMS ANY LIABILITY OF ANY KIND FOR ANY
//  DAMAGES WHATSOEVER RESULTING DIRECTLY OR INDIRECTLY FROM THE USE OF THIS SOFTWARE
//  OR OF ANY DERIVATIVE WORK.
//---------------------------------------------------------------------------------------

/* This (shared) window controller is all you need to search and fetch entrez. In comes 
with its own nib file and generates EntrezResult objects which are displayed in a 
tableview. We make use of NSConnection and NSURLDownload for asynchronous fetching of
results and sequences.
The flow of the program is in short:
- retrieve results for query, generates an xml file with IDs (using NCBI's eSearch eUtil).
- parse this file and retrieve summaries for each ID (using NCBI's eSummary's eUtil).
- parse xml file with summaries, create for each one a EntrezResult object and display 
  those in the Tableview.
- if the user selects one, it is fetched (using NCBI's eFetch eUtil).
- the response is parsed and forwarded to the delegate, this controller is cleaned-up 
  afterwards.
 
Improvements by Koen van der Drift:
- Now uses NSXMLParser
- Fetched data are stored in an NSDictionary, instead of EntrezResult for more flexibility
- Multiple records can be retrieved at once
- Several small fixes
 */

#import "BCEntrezController.h"
#import <SystemConfiguration/SystemConfiguration.h>

// The delegate only has to implement one method to receive the fetched sequences: 
@protocol BCEntrezControllerDelegate <NSObject>
- (void)importFetchedResults: (NSArray *)anArray;
@end


@implementation BCEntrezController  

//===========================================================================
#pragma mark -
#pragma mark � Init & Dealloc
//===========================================================================

- (BCEntrezController *)init
{
    self = [super initWithWindowNibName: @"Entrez"];
    prefs = [NSUserDefaults standardUserDefaults];
    
    return self;
}


- (void)awakeFromNib
{    
    searchResultsArray = [[NSMutableArray alloc] init];
    fetchedResultsArray = [[NSMutableArray alloc] init];

    [preview setFont: [NSFont fontWithName: @"Courier" size: 11.0]];

    [searchField setRecentsAutosaveName: @"RecentEntrezSearches"];
    [searchField setTarget: self];
    [searchField setDelegate: self];
    
    [self showPreview: self];
    
    [progressTextField setStringValue: @""];
    [self cleanupDownload];
    
    searchInProgress = NO;
    summaryFetchInProgress = NO;
    fetchInProgress  = NO;
    fetchedResultsCount = 0;

    receivedData = nil;
}

- (void)dealloc
{ 
    if(receivedData != nil)
    {
        [receivedData release];
        receivedData = nil;
    }
    
    if ( connection != nil ) 
    {
        [connection release];
        connection = nil;
    }
    
    [webenv release];
    [querykey release];
    
    [searchResultsArray release];
    [fetchedResultsArray release];
    [currentParseString release];
    [currentAttributeString release];
    [currentSearchResult release];
    
    [super dealloc];
}

-(void)setDelegate:(id) newDelegate
{
    delegate = newDelegate;
}

-(id)delegate 
{
    return delegate;
}


//===========================================================================
#pragma mark -
#pragma mark � ACTIONS
//===========================================================================

- (IBAction)searchForQuery:(id)sender       // initial query
{
        if([[sender stringValue]isEqualToString: @""])
    {
                [self cleanupDownload]; 
                [progressTextField setStringValue: @""];
                fetchedResultsCount = 0;
                [searchResultsArray removeAllObjects];
                [tv reloadData];
        }
    else if (searchInProgress || summaryFetchInProgress || fetchInProgress)
    {
     // cancel
        [self cleanupDownload];
        
     // go again
        [self retrieveSearchResultsForQuery: [sender stringValue]];
    } 
    else 
    {
     // search!
        [self retrieveSearchResultsForQuery: [sender stringValue]];
    }
}


- (IBAction)fetch:(id)sender    // get all the data from selected search result(s)
{
    NSIndexSet *is = [tv selectedRowIndexes];
    NSUInteger index = [is firstIndex];
    
    if (index == -1) 
    {
        NSBeep();
    }
    else 
    {   
	if (!fetchedResultsArray)
    {
        fetchedResultsArray = [[NSMutableArray alloc] init];     
    }
    else
    {
        [fetchedResultsArray removeAllObjects];
    }        
        while (index != NSNotFound)
        {
            [fetchedResultsArray addObject: 
             [searchResultsArray objectAtIndex: index]];
            
            index = [is indexGreaterThanIndex: index];
        }
        
        [self fetchResults: fetchedResultsArray];
    }
}


- (IBAction)cancel:(id)sender
{
    [self cleanupDownload];
    fetchedResultsCount = 0;
    [progressTextField setStringValue: @""];

    [NSApp endSheet: [self window] returnCode: 0];
    [[self window] orderOut: self];
}


- (IBAction)showPreview:(id)sender
{
    NSAttributedString* newStorage;
    NSMutableDictionary* attDict  = [NSMutableDictionary dictionary];

    NSInteger row = [tv selectedRow];
    
    NSMutableString *resultsstring = [NSMutableString stringWithCapacity: 1000];
    
    if (row == -1) 
    {
        [resultsstring appendString: @"\nNo Record Selected"];
        NSMutableParagraphStyle *modifiedStyle = [[NSMutableParagraphStyle alloc] init];
        [modifiedStyle setAlignment: NSCenterTextAlignment]; 
        [attDict setObject: modifiedStyle forKey: NSParagraphStyleAttributeName];
        [attDict setObject: [NSFont systemFontOfSize: 16.0] forKey: NSFontAttributeName];
        [attDict setObject: [NSColor lightGrayColor] forKey: NSForegroundColorAttributeName];
        [modifiedStyle release]; 
        
        newStorage = [[NSAttributedString alloc] initWithString: resultsstring attributes:attDict];
        [fetchButton setEnabled: NO];
        
    } else {
               
        [resultsstring appendString:   [NSString stringWithFormat: @"%@ %@", [[searchResultsArray objectAtIndex: row] objectForKey: @"extra"], [[searchResultsArray objectAtIndex: row] objectForKey: @"info"]]];
        
        [attDict setObject: [NSFont fontWithName: @"Courier" size: 12] forKey: NSFontAttributeName];
        [attDict setObject: [NSColor blackColor] forKey: NSForegroundColorAttributeName];
        [attDict setObject: [NSColor whiteColor] forKey: NSBackgroundColorAttributeName];
        
        newStorage = [[NSAttributedString alloc] initWithString: resultsstring attributes:attDict];
        [fetchButton setEnabled: YES];
    }
    
    [[preview textStorage] setAttributedString: newStorage];  
    [newStorage release];   
}


// ================================================================
#pragma mark -
#pragma mark � NSXMLPARSER DELEGATE METHODS
// ================================================================


-(void)parser:(NSXMLParser *)p didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if (summaryFetchInProgress)
    {
        if ([elementName isEqualToString: @"Item"])
        {
            if (!currentAttributeString)
            {
                currentAttributeString = [[NSMutableString alloc] init];
            }

            [currentAttributeString setString: [attributeDict objectForKey: @"Name"]];
        }
    }
    
    if (fetchInProgress)
    {
        if ([elementName isEqualToString: @"TSeqSet"])
        {
            if (!fetchedResultsArray)	
            {
                fetchedResultsArray = [[NSMutableArray alloc] init];
            }
        }

        if ([elementName isEqualToString: @"TSeq"])
        {
            if (!currentSearchResult)
            {
                currentSearchResult = [[NSMutableDictionary alloc] init];
            }

            currentSearchResult = [fetchedResultsArray objectAtIndex: fetchedSummaryCounter];
            fetchedSummaryCounter++;
        }
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (!currentParseString) 
    {
        currentParseString = [[NSMutableString alloc] initWithCapacity:50];
    }
  
// remove newline and tab characters  
    [currentParseString appendString: [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
} 


-(void)parser:(NSXMLParser *)p didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (searchInProgress)
    {       
        if ([elementName isEqualToString: @"WebEnv"])
        {
            [self setWebenv: currentParseString];		// [databaseController setWebEnv: currentParseString];
        }
        
        if ([elementName isEqualToString: @"QueryKey"])
        {
            [self setQuerykey: currentParseString];		// [databaseController setQueryKey: currentParseString];
        }
        
        if ([elementName isEqualToString: @"Count"])
        {
            searchCount = [currentParseString intValue]; 	// [databaseController setSearchCount: currentParseString];
        }
    }
    
    if (summaryFetchInProgress)
    {
        if ([elementName isEqualToString: @"Id"])
        {
            if (!currentSearchResult)
            {
                currentSearchResult = [[NSMutableDictionary alloc] init];
            }
            
		[currentSearchResult setObject: currentParseString forKey:@"id"];
        }
        
        if ([elementName isEqualToString: @"Item"])
        {
            if ([currentAttributeString isEqualToString: @"Caption"])
            {
                [currentSearchResult setObject: currentParseString forKey: @"accession"];
            }

            if ([currentAttributeString isEqualToString: @"Extra"])
            {
                [currentSearchResult setObject: currentParseString forKey: @"info"];
            }
            
            if ([currentAttributeString isEqualToString: @"Title"])
            {
                [currentSearchResult setObject: currentParseString forKey: @"name"];
            }

            if ([currentAttributeString isEqualToString: @"TaxId"])
            {
                [currentSearchResult setObject: currentParseString forKey: @"species"];
            }
         }
        
        if ([elementName isEqualToString: @"DocSum"])
        {
         // before we add currentSearchResult to the searchResultsArray, let's add an url based on the accession number:       

		NSMutableString *url = [[NSMutableString alloc] initWithString: @"http://www.ncbi.nlm.nih.gov/protein/"];
            [url appendString: [currentSearchResult objectForKey: @"accession"]];
            [currentSearchResult setObject: url forKey:@"url"];
                
            [url release];
            url = nil;

            [searchResultsArray addObject: currentSearchResult];

	   // we're done with the currenSearchResult
            [currentSearchResult release];
            currentSearchResult = nil;
        }
    }
    
    if (fetchInProgress)
    {
        if ([elementName isEqualToString: @"TSeq_sequence"])
        {
            [currentSearchResult setObject: currentParseString forKey:@"sequence"];
     //       currentSearchResult = nil;		// <<-----  is this ok??
        }
    }
    
    [currentParseString release];
    currentParseString = nil;

    [currentAttributeString release];
    currentAttributeString = nil;
}



// ================================================================
#pragma mark -
#pragma mark � DOWNLOAD METHODS
// ================================================================

- (void)retrieveSearchResultsForQuery: (NSString *)q
{    
    if([[searchField stringValue] isEqualToString: @""])
    {
        NSBeep();
        return;
    }
    
    // PREPARE
    [progress startAnimation: self];
    [progressTextField setStringValue: @"Contacting NCBI..."];
    
    searchInProgress = YES;
    
    /*
     // PREVIOUS SEARCH?  -> support for Search more using webenv, not implemented here...
     if(fetchedResultsCount > 0 && webenv != nil){
     } else {
     */
	//NSLog(@"%@", q);
    
    NSMutableString *str = [NSMutableString stringWithString: [NSString stringWithFormat:
                                                               @"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=protein&retmode=xml&usehistory=y&retmax=150&retstart=%d&term=", fetchedResultsCount]];
    
	NSMutableString *query = [NSMutableString stringWithString: q];
    
    // Replace spaces by +
    [query replaceOccurrencesOfString: @" " withString: @"+" 
                              options:  NSCaseInsensitiveSearch 
                                range: NSMakeRange(0, [query length])];
	
    [str appendString: query];
    
    [self retrieveRequestedDataFromQuery: str];
}


- (BOOL)parseSearchResults: (NSData *)results
{
    NSError* err;
    searchCount = 0;

    BOOL success = [self parseData:results intoArray: nil error: &err];
    
    [self cleanupDownload];
    return success;
}


- (BOOL)parseData: (NSData *)results intoArray: (NSArray *)array error: (NSError **)err
{

    NSXMLParser *parser = [[NSXMLParser alloc] initWithData: results];
    [parser setDelegate: self];
    
    BOOL success = [parser parse];
    
    if (!success && err != NULL)
        *err = [parser parserError];
    
    [parser release];
    return success;
}


- (void)retrieveSummaries
{
    // PREPARE
    //[progress setIndeterminate: YES];
    [progress startAnimation: self];
    [progressTextField setStringValue: @"Receiving results from NCBI..."];
 
    summaryFetchInProgress = YES;
    
    NSString *url = [NSString stringWithFormat: 
            @"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=protein&retmode=xml&retmax=150&retstart=%d&WebEnv=%@&query_key=%@", 
                       fetchedResultsCount, [self webenv], [self querykey]];

    //NSLog(@"Retrieve: %@", url);
    [self retrieveRequestedDataFromQuery: url];
}


- (BOOL)parseSummaries: (NSData *)results
{
    NSError* err;
    
    [searchResultsArray removeAllObjects];
    fetchedSummaryCounter = 0;

    BOOL success = [self parseData:results intoArray: nil error: &err];
    
    if (success)    // DO THESE KIND OF CHECKS!
    {
        [tv reloadData];
        [self showPreview: self];
    }

    [self cleanupDownload];

//    [searchResultsArray sortUsingSelector: @selector(sortResultsOnIdAscending:)];
//
//    if(success && searchCount > fetchedResultsCount + 50){
//         fetchedResultsCount += 50;
//         //[searchButton setTitle: @"More"];
//     } else {
//         fetchedResultsCount = 0;
//         //[searchButton setTitle: @"Search"];
//     }

    return success;
}


- (void)fetchResults:(NSMutableArray *)resultsToBeFetched
{
    NSMutableString *idString = [[NSMutableString alloc] init];
    
    for (NSDictionary *result in resultsToBeFetched)
    {
        [idString appendString: [result objectForKey: @"accession"]];
        [idString appendString: @","];
    }
   
 // remove the trailing comma 
    [idString replaceCharactersInRange:NSMakeRange([idString length]-1, 1) withString:@""];
    
    [progress startAnimation: self];
    [progressTextField setStringValue: @"Retrieving records..."];
    
    fetchInProgress = YES;
    
    NSString *query = [NSString stringWithFormat: 
				@"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id=%@&retmode=xml&rettype=fasta", idString];
    
    [self retrieveRequestedDataFromQuery: query];
    [idString release];
}


- (BOOL)parseFetch: (NSData *)results
{
    NSError* err;
        
    BOOL success = [self parseData:results intoArray: nil error: &err];
    
    if (success)    // DO THESE KIND OF CHECKS!
    {
     // we have all of our records, lets send it to our delegate
//        id <BCEntrezControllerDelegate> del = [self delegate];
//        if ([del respondsToSelector:@selector(importFetchedResults:)])
//        {
//		[del importFetchedResults: fetchedResultsArray]; 
//        }
    }
    
    [self cleanupDownload];

  // bye    
    [NSApp endSheet: [self window] returnCode: 1];
    [[self window] orderOut: self];

    return success;
}


- (void)cleanupDownload
{        
    [progress stopAnimation: self];
    //[progress setIndeterminate: NO];
    //[progress setDoubleValue: 0.0];
    //[progressTextField setStringValue: @""];
    
    searchInProgress = NO;
    summaryFetchInProgress = NO;
    fetchInProgress = NO;
    
    if(receivedData != nil)
    {
        [receivedData release];
        receivedData = nil;
    }
    
    if ( connection != nil )
    {
        [connection cancel];
        [connection release];
        connection = nil;
    }
    
    if ( response != nil )
    {
        [response release];
        response = nil;
    }
}

- (void)reportDownloadFailureWithError: (NSString *)errorstring
{
}

- (void) NCBIconnectionError: (id) anError
{
}

-(void)retrieveRequestedDataFromQuery: (NSString *)query
{
    if(!query || [query isEqualToString: @""])
    {
        NSBeep();
        [progressTextField setStringValue: @"Error while retrieving summaries. Please try again."];
        [self cleanupDownload];
        return;   
    }
    
    if ( ![self _canConnect: @"http://eutils.ncbi.nlm.nih.gov/"] ) 
    {
        [self NCBIconnectionError: @"not reachable"]; 
        [progressTextField setStringValue: @"Unable to contact NCBI. Please provide an internet connection."];
        return;
    }
    
    receivedData = [[NSMutableData alloc] init];
    
    NSURLRequest *theRequest = [NSURLRequest requestWithURL: [NSURL URLWithString: query]
                                                cachePolicy:  NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:  20.0];
    if ( !theRequest) 
    {
        [progressTextField setStringValue: @"Unable to generate query. Check for any inapropriate characters in your query."];
        [self NCBIconnectionError: @"no request"]; 
        return;
    }
    
    connection = [NSURLConnection connectionWithRequest: theRequest delegate:self];
    
    if ( !connection ) 
    {
        [progressTextField setStringValue: @"Unable to generate query. Check for any inapropriate characters in your query."];
        [self NCBIconnectionError: @"connection failure"];
        return;
    }
    
    [connection retain];
}


// ================================================================
#pragma mark -
#pragma mark � DOWNLOAD ACCESSORS
// ================================================================

- (NSURLResponse *)response
{
    return response;
}

- (void)setResponse:(NSURLResponse *)newResponse
{
    [newResponse retain];
    [response release];
    response = newResponse;
}

- (NSString *)webenv
{
        return webenv;
}

- (void)setWebenv:(NSString *)newWebenv
{
        [newWebenv retain];
        [webenv release];
        webenv = newWebenv;
}

- (NSString *)querykey
{
        return querykey;
}

- (void)setQuerykey:(NSString *)newQuerykey
{
        [newQuerykey retain];
        [querykey release];
        querykey = newQuerykey;
}

- (NSDictionary *)currentSearchResult
{
    return currentSearchResult;
}

- (NSArray *)fetchedResultsArray
{
    return fetchedResultsArray;
}

//===========================================================================
#pragma mark -
#pragma mark � TABLEVIEW METHODS
//===========================================================================


- (NSInteger)numberOfRowsInTableView:(NSTableView *)theTableView
{
    return [searchResultsArray count];
}

- (id)tableView:(NSTableView *)theTableView objectValueForTableColumn:(NSTableColumn *)theColumn row:(NSInteger)rowIndex
{       
    if([[theColumn identifier]isEqualToString: @"accession"])
    {
        return [[searchResultsArray objectAtIndex: rowIndex] objectForKey: @"accession"];
        
    } 
    
    else if([[theColumn identifier]isEqualToString: @"name"])
    {
        return [[searchResultsArray objectAtIndex: rowIndex] objectForKey: @"name"];
        
    } 

    return nil;
    
}

- (void)tableView:(NSTableView *)aTableView willDisplayCell:(id)aCell forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
    // TO COMPENSATE FOR ADJUSTED iTABLEVIEW
    if([(NSCell *)aCell type] == NSTextCellType)
    {
        [aCell setTextColor: [NSColor blackColor]];
    }
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
    [self showPreview: self];
}

- (void)tableView:(NSTableView*)tv didClickTableColumn:(NSTableColumn *)tableColumn
{
}

- (void)clearIndicatorImages
{    
}


// ================================================================
#pragma mark -
#pragma mark � CONNECTION DELEGATES
// ================================================================

- (void) connection: (NSURLConnection *)theConnection didReceiveResponse: (NSURLResponse *)theresponse
{
    // NSLog(@"Response: %@", theresponse);
    
    // Apple says to clear the data in the case of a redirect
    // we pretty much trust Apple on this
    [receivedData setLength: 0];
    
    // retain the response to use later
    [self setResponse: theresponse];
        
    bytesReceived = 0;
    
    [progressTextField setStringValue: @"Connected to NCBI..."];
}

- (void) connection: (NSURLConnection *)connection didReceiveData: (NSData *)data 
{
    //NSLog(@"Bytes received - %d", [data length]);

    // append the new data to the receivedData
    [receivedData appendData:data];

    bytesReceived = bytesReceived + [data length];

        if(searchInProgress) 
        [progressTextField setStringValue: [NSString stringWithFormat: 
            @"Receiving results from NCBI... (%dKb)", bytesReceived/1024]];
        
    else if (summaryFetchInProgress) 
        [progressTextField setStringValue: [NSString stringWithFormat:
            @"Receiving record from NCBI... (%dKb)", bytesReceived/1024]];
}

- (void)connection: (NSURLConnection *)connection didFailWithError: (NSError *)error 
{
    // forward the actual error to our error generation routine
    [self NCBIconnectionError: error];
    [progressTextField setStringValue: [NSString stringWithFormat: @"Error: %@", [error localizedDescription]]];

    [self cleanupDownload];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection 
{    
    //NSLog(@"Received: %@", [NSString stringWithUTF8String: [receivedData bytes]]);
    
    if(searchInProgress)
    {
        if([self parseSearchResults: receivedData])
        {
            [self cleanupDownload];
                        [progressTextField setStringValue: @"Parsing received records..."];
            [self retrieveSummaries];
        } 
        else 
        {
            [self NCBIconnectionError: @"error parsing search results"];
            [progressTextField setStringValue: @"No results found."];
            [self cleanupDownload];
        }
    } 
    else if (summaryFetchInProgress)
    {
        if([self parseSummaries: receivedData])
        {
            [self cleanupDownload];
            [progressTextField setStringValue: [NSString stringWithFormat: @"%d records found.", searchCount]];
        } 
        else 
        {
            [self NCBIconnectionError: @"error parsing summaries"];
            [progressTextField setStringValue: @"No results found."];
            [self cleanupDownload];
        }
        
    } 
    else if (fetchInProgress)
    {
        if([self parseFetch: receivedData])
        {
            [self cleanupDownload];
            [progressTextField setStringValue: @""];
        } 
        else 
        {
            [self NCBIconnectionError: @"error parsing record"];
            [progressTextField setStringValue: @"Error parsing record."];
            [self cleanupDownload];
        }        
    }
}



//===========================================================================
#pragma mark -
#pragma mark � GENERAL METHODS
//===========================================================================

- (BOOL)parseOutput: (NSData *)output
{
        // Debugging method to parse the output, disabled here.
/*
    SXMLTree *xmldata, *theXMLHitSet;
    NS_DURING
        xmldata = [[SXMLTree alloc] initFromData: output usingEncoding:NSASCIIStringEncoding];
    NS_HANDLER
        NSLog(@"Error initializing SXMLTree from output");
    NS_ENDHANDLER
    
    if([xmldata findRoot] == nil) return NO;
    
    [self setResults: xmldata]; 
    [xmldata release];
    
    //TEST
    //Fetch important data from the XML entry
    //SXMLTree *theXMLOutput = [results childWithPath:@"/BlastOutput"];
    NS_DURING
        theXMLHitSet = [[self results] childWithPath: @"/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits"];
    NS_HANDLER
        NSLog(@"Error initializing SXMLTree from output");
    NS_ENDHANDLER
    
    int theHitCount  = [theXMLHitSet childCount];
    int i;
    for (i=0; i< theHitCount; i++) {
        NS_DURING
            SXMLTree *theXMLHit = [theXMLHitSet childAtIndex:i];
        NS_HANDLER
            NSLog(@"Error initializing SXMLTree from output");
        NS_ENDHANDLER
        //NSLog(@"%@", [[theXMLHit childWithPath: @"Hit_def"]nodeText]);
    }
    
    // runstatistics
    
    // results
    
    // querystatistics
    return [self resultsAvailable];
     */
    return YES;
}


- (BOOL)validateMenuItem:(NSMenuItem *)anItem
{
    if([[anItem title] isEqualToString: @"Recent Searches"])
                return NO;
    else 
        return YES;
}
                

/////////////////////////////////////////////////////
// a private method to determine network availability
/////////////////////////////////////////////////////
- (BOOL) _canConnect:(NSString *)url
{
    Boolean canConnect = NO; // assume the worst...

    SCNetworkReachabilityRef target;
    SCNetworkConnectionFlags flags = 0;

    target = SCNetworkReachabilityCreateWithName(NULL, [url UTF8String]);
    canConnect = SCNetworkReachabilityGetFlags(target, &flags);
    CFRelease(target);
    
    return canConnect;
}

@end