//
//  BCGEODownloadController.m
//  BioCocoa
//
//  Created by Scott Christley on 10/07/08.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCGEODownloadController.h"
#import "BCPreferences.h"
#import "BCGeneExpressionOmnibus.h"
#import "BCSeries.h"

#import <ApplicationServices/ApplicationServices.h>

static BCGEODownloadController *sharedController = nil;

@implementation BCGEODownloadController

+ sharedGEODownload
{
  if (!sharedController) sharedController = [BCGEODownloadController new];
  return sharedController;
}

// Default constructor
- init
{
  self = [super init];
  if (self) {
    series = [[NSMutableArray array] retain];
    
    [NSBundle loadNibNamed: @"BCGEODownload" owner: self];
  }
  return self;
}

- (void)dealloc
{
  if (series) [series release];
  [super dealloc];
}

- (void)awakeFromNib
{
	// update the table view
	[seriesTable setDataSource: self];
} 

- (void)makeKeyAndOrderFront: (id)sender
{
	[downloadPanel makeKeyAndOrderFront: sender];
}

// GUI Interaction
- (IBAction)downloadSeries: (id)sender
{
  NSString *s = [downloadSeries stringValue];
  if (!s) return;
  if ([s length] == 0) return;
  
  BCSeries *aSeries = [BCSeries seriesWithId: s];
  if (aSeries) {
    NSString *msg = [NSString stringWithFormat: @"GEO Series (%@) has already been downloaded.  Are you sure that you want to download again?", s];
    int ret = NSRunAlertPanel(@"Download GEO Series", msg, @"Cancel", @"Download", nil);
    if (ret == NSAlertDefaultReturn) return;
  }
  
  BCGeneExpressionOmnibus *geo = [BCGeneExpressionOmnibus new];
  [geo downloadGEOSeries: s];
}

- (IBAction)retrieveList: (id)sender
{
  int ret = NSRunAlertPanel(@"Retrieve List of All Series from GEO",
    @"Retrieving the list of all series from GEO may take a few minutes depending upon your network connection.  Are you sure that you want to retrieve the list?",
    @"Retrieve", @"Cancel", nil);
  if (ret == NSAlertAlternateReturn) return;

  NSMutableArray *a = [BCGeneExpressionOmnibus retrieveGEOSeriesList];
  if (a) {
    [series release];
    series = [a retain];
  }
  [seriesTable reloadData];
}

- (IBAction)showSeriesInfo: (id)sender
{
  int i = [seriesTable selectedRow];
  if (i == -1) return;

  NSString *s = @"http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=";
  s = [s stringByAppendingString: [series objectAtIndex: i]];
	NSURL *anURL = [NSURL URLWithString: s];
	LSOpenCFURLRef((CFURLRef)anURL, NULL);
}

//
// Manage the table of series
//
- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
  return [series count];
}

- (id)tableView:(NSTableView *)aTableView
    objectValueForTableColumn:(NSTableColumn *)aTableColumn
            row:(int)rowIndex
{
  id theRecord;
  
	//printf("tableView:objectValueForTableColumn:row:\n");
  NSParameterAssert(rowIndex >= 0 && rowIndex < [series count]);
  theRecord = [series objectAtIndex:rowIndex];
  return theRecord;
}

//
// NSTableView delegate methods
//
- (void)tableViewSelectionDidChange:(id)sender
{
	printf("tableViewSelectionDidChange:\n");
  int i = [seriesTable selectedRow];
  if (i == -1) return;

  [downloadSeries setStringValue: [series objectAtIndex: i]];
}

@end
