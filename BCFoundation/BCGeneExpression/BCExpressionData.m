//
//  BCExpressionData.m
//  BioCocoa
//
//  Created by Scott Christley on 3/24/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BCExpressionData.h"
#import "BCParseSOFT.h"
#import "BCPlatform.h"
#import "BCSample.h"

@implementation BCExpressionData

- initWithExpressionFile:(NSString *)aFile
{
  [super init];
  
  expressionFile = aFile;
  ignoreMissingGenes = NO;
  
  BCParseSOFT *parser1 = [BCParseSOFT new];
  printf("Parsing file: %s\n", [expressionFile UTF8String]);
  [parser1 parseFile: expressionFile];
    
  return self;
}

- (void)setIgnoreMissingGenes: (BOOL)aFlag { ignoreMissingGenes = aFlag; }
- (BOOL)ignoreMissingGenes { return ignoreMissingGenes; }

- (BCDataMatrix *)extractDatasetForGenes:(NSArray *)geneList fromSamples:(NSArray *)sampleList
                              probesUsed:(NSArray **)probesUsed platformTag: (NSString *)aTag
{
  int i, j;
  
  // Get list of platforms from the sample list
  NSString *platformID;
  for (i = 0; i < [sampleList count]; ++i) {
    BCSample *s = [BCSample sampleWithId: [sampleList objectAtIndex: i]];
    platformID = [s valueForKey: @"Sample_platform_id"];
    printf("%s\n", [platformID UTF8String]);
  }

  // 1 platform
  BCPlatform *p1 = [BCPlatform platformWithId: platformID];
  NSArray *probeSet = [p1 valuesForKey: @"Probe Set"];
  NSArray *probeSetColumns = [p1 valuesForKey: @"Probe Set Columns"];
  //printf("%s\n", [[probeSetColumns description] UTF8String]);
  NSUInteger colIndex = [probeSetColumns indexOfObject: aTag];
  if (colIndex == NSNotFound) return nil;
  
  // construct genes to probes map for each platform

  //
  // map genes to probes
  //
  NSMutableArray *columnNames = [NSMutableArray array];
  NSMutableArray *rowNames = [NSMutableArray array];
  NSMutableArray *geneMap = [NSMutableArray array];
  for (j = 0; j < [geneList count]; ++j) {
    NSString *geneID = [geneList objectAtIndex: j];
    NSMutableArray *probeList = [NSMutableArray array];
    for (i = 0; i < [probeSet count]; ++i) {
      NSArray *a1 = [probeSet objectAtIndex: i];
      NSString *probeGene = [a1 objectAtIndex: colIndex];
      //printf("%s %s %s\n", [[a1 objectAtIndex: 9] UTF8String], [[a1 objectAtIndex: 10] UTF8String], [[a1 objectAtIndex: 11] UTF8String]);
      
      NSRange r1 = [probeGene rangeOfString: @" "];
      if (r1.location != NSNotFound) {
        r1.length = r1.location;
        r1.location = 0;
        probeGene = [probeGene substringWithRange: r1];
      }
      //printf("%s %s\n", [[a1 objectAtIndex: 8] UTF8String], [probeGene UTF8String]);
      
      if ([geneID isEqualToString: probeGene]) {
        //printf("%s %s\n", [probeGene UTF8String], [[a1 objectAtIndex: 0] UTF8String]);
        [probeList addObject: [NSNumber numberWithInt: i]];
      }
    }
    
    if ([probeList count] == 0) {
      if (!ignoreMissingGenes) {
        fprintf(stderr, "ERROR: gene does not map to any probes: %s\n", [geneID UTF8String]);
        return nil;
      }
    } else {
      [geneMap addObject: probeList];
      [columnNames addObject: geneID];
    }
  }
  
  // Put expression data into data matrix
  int numOfVariables = [columnNames count];
  int numOfObservations = [sampleList count];
  BCDataMatrix *dMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfObservations andColumns: numOfVariables andEncode: BCdoubleEncode];
  [dMatrix setColumnNames: columnNames];
  [dMatrix setRowNames: rowNames];
	double (*DM)[numOfObservations][numOfVariables];
	DM = [dMatrix dataMatrix];
  
  //printf("%d observations\n", numOfObservations);
	//printf("%d variables\n", numOfVariables);

  // for each gene
  for (j = 0; j < [columnNames count]; ++j) {
    //NSString *geneID = [geneList objectAtIndex: j];
    //NSString *geneName = [geneNameList objectAtIndex: j];
    NSArray *probeList = [geneMap objectAtIndex: j];

    // for each sample
    int bestProbe = -1;
    for (i = 0; i < [sampleList count]; ++i) {
      NSString *sampleID = [sampleList objectAtIndex: i];
      BCSample *aSample = [BCSample sampleWithId: sampleID];
      NSArray *sampleData = [aSample valuesForKey: @"Sample Data"];

      if (!sampleData) printf("WARNING: No sample data for sample: %s\n", [sampleID UTF8String]);

      (*DM)[i][j] = [self bestProbeValue:&bestProbe forProbeSet:probeSet probeList:probeList sampleData:sampleData];
      if (j == 0) [rowNames addObject: sampleID];
      //printf("%s %f\n", [[columnNames objectAtIndex: j] UTF8String], (*DM)[i][j]);

      //int probeIndex = [[probeList objectAtIndex: bestProbe] intValue];
      //NSArray *a1 = [probeSet objectAtIndex: probeIndex];
      //NSString *probeName = [a1 objectAtIndex: 0];
      //printf("%s\t%s\t%s", [geneID UTF8String], [geneName UTF8String], [probeName UTF8String]);
    }      
  }
  
  return dMatrix;
}

- (BCDataMatrix *)extractDatasetForProbes:(NSArray *)probeList fromSamples:(NSArray *)sampleList
{
  return nil;
}

- (double)bestProbeValue:(int *)bestProbe forProbeSet:(NSArray *)probeSet probeList:(NSArray *)probeList sampleData:(NSArray *)sampleData
{
  int l, k;
  double firstValue = 0.0;
  BOOL first = YES;
  BOOL found = NO;
  int firstProbe;
  double value = 0.0;
  
  for (l = 0; l < [probeList count]; ++l) {
    int probeIndex = [[probeList objectAtIndex: l] intValue];
    NSArray *a1 = [probeSet objectAtIndex: probeIndex];
    NSString *probeName = [a1 objectAtIndex: 0];
    
    for (k = 0; k < [sampleData count]; ++k) {
      NSArray *aData = [sampleData objectAtIndex: k];
      
      if ([probeName isEqualToString: [aData objectAtIndex: 0]]) {
        value = [[aData objectAtIndex: 1] doubleValue];
        if ([aData count] > 2) {
          NSString *presentFlag = [aData objectAtIndex: 2];
          if (![presentFlag isEqualToString: @"P"]) {
            value = 0.0;
          }
        }
        if (first) {
          firstValue = value;
          first = NO;
          firstProbe = l;
        }
        NSRange r = [probeName rangeOfString: @"_s_"];
        if (r.location != NSNotFound) {
          found = YES;
          *bestProbe = l;
          break;
        }
      }
    }
    
    if (found) break;
  }
  
  // if did not find _s_ probe then take first probe
  if (!found) {
    value = firstValue;
    *bestProbe = firstProbe;
  }
  
  return value;
}

@end
