//
//  BCCachedSequence.m
//  BioCocoa
//
//  Created by Scott Christley on 7/12/08.
//  Copyright (c) 2008-2010 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCCachedSequence.h"
#import "BCSymbolSet.h"
#import <Foundation/NSException.h>

#define NOT_IMPLEMENTED [NSException raise: @"method not implemented by BCCachedSequence" format: nil]

@implementation BCCachedSequence

- initWithCachedSequenceFile: (BCCachedSequenceFile *)aFile
{
    if ( (self = [super initWithString: nil symbolSet: [BCSymbolSet unknownSymbolSet]]) ) {
		cacheFile = aFile;
	}
	
	return self;
}

//
// Not implemented for cached sequences
//
- (id)initWithSymbolArray:(NSArray *)anArray symbolSet:(BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
- (id)initWithData:(NSData *)aData symbolSet:(BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
- (id)initWithSymbolArray:(NSArray *)anArray { NOT_IMPLEMENTED; return nil; }
- (id)initWithString:(NSString *)aString symbolSet:(BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
- (id)initWithString:(NSString*)aString { NOT_IMPLEMENTED; return nil; }
- (id)initWithString:(NSString*)aString range:(NSRange)aRange { NOT_IMPLEMENTED; return nil; }
- (id)initWithString:(NSString*)aString range:(NSRange)aRange symbolSet:(BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
- (id)initWithThreeLetterString:(NSString*)aString symbolSet:(BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
+ (BCSequence *)sequenceWithString:(NSString *)aString { NOT_IMPLEMENTED; return nil; }
+ (BCSequence *)sequenceWithString:(NSString *)aString symbolSet:(BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
+ (BCSequence *)sequenceWithThreeLetterString:(NSString *)aString symbolSet:(BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
+ (BCSequence *)sequenceWithSymbolArray:(NSArray *)entry { NOT_IMPLEMENTED; return nil; }
+ (BCSequence *)sequenceWithSymbolArray:(NSArray *)entry symbolSet: (BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }
+ (BCSequence *)objectForSavedRepresentation:(NSString *)sequence { NOT_IMPLEMENTED; return nil; }
- (NSString *)convertThreeLetterStringToOneLetterString:(NSString *)aString symbolSet: (BCSymbolSet *)aSet { NOT_IMPLEMENTED; return nil; }

- (BCSequenceType)sequenceTypeForString:(NSString *)string { NOT_IMPLEMENTED; return BCSequenceTypeOther; }
- (BCSequenceType)sequenceTypeForData:(NSData *)aData { NOT_IMPLEMENTED; return BCSequenceTypeOther; }
- (BCSequenceType)sequenceTypeForSymbolArray:(NSArray *)anArray { NOT_IMPLEMENTED; return BCSequenceTypeOther; }

- (NSData *) sequenceData { NOT_IMPLEMENTED; return nil; }
- (const unsigned char *) bytes { NOT_IMPLEMENTED; return NULL; }
- (NSString *)sequenceString { NOT_IMPLEMENTED; return nil; }

- (void)removeSymbolsInRange:(NSRange)aRange { NOT_IMPLEMENTED; }
- (void)removeSymbolAtIndex:(int)index { NOT_IMPLEMENTED; }
- (void)insertSymbolsFromSequence:(BCSequence *)seq atIndex:(int)index { NOT_IMPLEMENTED; }

- (BCSequence *) reverse { NOT_IMPLEMENTED; return nil; }
- (BCSequence *) complement { NOT_IMPLEMENTED; return nil; }
- (BCSequence *) reverseComplement { NOT_IMPLEMENTED; return nil; }

@end
