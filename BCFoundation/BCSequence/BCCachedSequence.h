//
//  BCCachedSequence.h
//  BioCocoa
//
//  Created by Scott Christley on 7/12/08.
//  Copyright (c) 2008-2010 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/*!
@header
@abstract Class that holds a biological sequence (DNA, protein, etc.) cached in memory and stored on disk.
*/

#import <Foundation/Foundation.h>
#import <BioCocoa/BCSequence.h>

@class BCCachedSequenceFile;

/*!
    @class      BCCachedSequence
    @abstract   Class that holds a biological sequence (DNA, protein, etc.) cached in memory and stored on disk.
    @discussion When dealing with large sequences such as whole chromosomes, genomes or
		even multiple genomes, it is not possible to load the whole sequence into memory.
		This class provides access to most read-only BCSequence functionality
		while maintaining the sequence on disk, uses BCCachedSequenceFile for disk access.
		
		Do not actually allocate instances of BCCachedSequence in your code, instead use
		BCSequenceReader to read a file as cached.
*/

@interface BCCachedSequence : BCSequence {
	BCCachedSequenceFile *cacheFile;
	NSDictionary *sequenceInfo;
}

/*!
	@abstract   initializes a BCCachedSequence object.
*/
- initWithCachedSequenceFile: (BCCachedSequenceFile *)aFile andInfo:(NSDictionary *)aDictionary;


/*!
    @abstract   returns the symbolSet associated with this sequence
*/
- (BCSymbolSet *) symbolSet;


/*!
	@abstract   sets the symbolSet associated with this sequence
*/
- (void) setSymbolSet:(BCSymbolSet *)symbolSet;


/*!
    @abstract   returns the type of sequence
*/
- (BCSequenceType) sequenceType;


/*!
	@abstract   returns the length of the sequence
*/
- (unsigned int) length;


/*!
    @method     symbolAtIndex: (int)theIndex
    @abstract   returns the BCSymbol object at index.  Returns nil if index is out of bounds.
*/
- (BCSymbol *)symbolAtIndex: (int)theIndex;




/*!
    @method     containsAmbiguousSymbols
    @abstract   determines whether any symbols in the sequence are compound symbols.
*/
- (BOOL) containsAmbiguousSymbols;



/*!
    @method    symbolArray
      @abstract   returns a ponter to the array of BCSymbol objects that make up the sequence.
      @discussion The array returned is the object used internally by BCSequence.
	   The array obtained should only be used for reading, and should not be modified by the caller.	 
     To modify the sequence, instead use one of the
	   convenience methods setSymbolArray, removeSymbolsInRange, removeSymbolsAtIndex,
	   or insertSymbolsFromSequence:atIndex. 
*/
- (NSArray *)symbolArray;


/*!
    @method    clearSymbolArray
    @abstract   clears the symbol array
    @discussion Removes all symbols from the symbol array and sets the array to nil.
       Call this method first to re-generate the symbolArray after the sequence has been modified.
*/
- (void)clearSymbolArray;

/*!
    @method    subSequenceStringInRange:(NSRange)aRange
    @abstract   returns a subsequence in string form
    @discussion returns an NSString containing a subsequence specified by aRange (0-based).
       Returns nil if any part of aRange is out of bounds.
*/
- (NSString *)subSequenceStringInRange:(NSRange)aRange;

/*!
    @method    sequenceStringFromSymbolArray:(NSArray *)anArray
    @abstract   returns an NSString representation of a symbolArray
*/
- (NSString *)sequenceStringFromSymbolArray:(NSArray *)anArray;

/*!
    @method    subSymbolArrayInRange:(NSRange)aRange
    @abstract   returns a sub-symbolarray form
    @discussion returns an NSArray containing a subsequence specified by aRange (0-based).
       Returns nil if any part of aRange is out of bounds.
*/
- (NSArray *)subSymbolArrayInRange:(NSRange)aRange;

/*!
    @method    subSequenceInRange:(NSRange)aRange
    @abstract   returns a sub-symbollist
    @discussion returns an BCSequence containing a sub-symbollist specified by aRange (0-based).
       Returns nil if any part of aRange is out of bounds.
*/
- (BCSequence *)subSequenceInRange:(NSRange)aRange;


/*!
     @method     savableRepresentation
     @abstract   Returns the sequenceString for saving.
     @discussion All BCSymbol classes implement this method to provide a standard way of
       accessing their data in a format that can be stored in Apple .plist files within
       arrays or dictionaries.
*/
- (NSString *) savableRepresentation;


/*!
     @method     description
     @abstract   Overrides NSObject's description - describes the object in string form
     @discussion In the default implementation, returns the sequence string.  Useful primarily
       for debugging.
*/
- (NSString *) description;



/*!
    @method    setSymbolArray:(NSArray *)anArray
    @abstract   sets the symbollist as an array of BCSymbol objects.
*/
- (void)setSymbolArray:(NSArray *)anArray;





#if 0
#pragma mark == FINDING SUBSEQUENCES ==
#endif


- (NSArray *) findSequence: (BCSequence *) entry;
- (NSArray *) findSequence: (BCSequence *) entry usingStrict: (BOOL) strict;
- (NSArray *) findSequence: (BCSequence *) entry usingStrict: (BOOL) strict firstOnly: (BOOL) firstOnly;
- (NSArray *) findSequence: (BCSequence *) entry usingStrict: (BOOL) strict firstOnly: (BOOL) firstOnly usingSearchRange: (NSRange) range;


#if 0
#pragma mark == NOT SUPPORTED ==
#endif
/*  These BCSequence methods are not supported in BCCachedSequence
	and will throw an exception if called.
*/

- (id)initWithSymbolArray:(NSArray *)anArray symbolSet:(BCSymbolSet *)aSet;
- (id)initWithData:(NSData *)aData symbolSet:(BCSymbolSet *)aSet;
- (id)initWithSymbolArray:(NSArray *)anArray;
- (id)initWithString:(NSString *)aString symbolSet:(BCSymbolSet *)aSet;
- (id)initWithString:(NSString*)aString;
- (id)initWithString:(NSString*)aString range:(NSRange)aRange;
- (id)initWithString:(NSString*)aString range:(NSRange)aRange symbolSet:(BCSymbolSet *)aSet;
- (id)initWithThreeLetterString:(NSString*)aString symbolSet:(BCSymbolSet *)aSet;
+ (BCSequence *)sequenceWithString:(NSString *)aString;
+ (BCSequence *)sequenceWithString:(NSString *)aString symbolSet:(BCSymbolSet *)aSet;
+ (BCSequence *)sequenceWithThreeLetterString:(NSString *)aString symbolSet:(BCSymbolSet *)aSet;
+ (BCSequence *)sequenceWithSymbolArray:(NSArray *)entry;
+ (BCSequence *)sequenceWithSymbolArray:(NSArray *)entry symbolSet: (BCSymbolSet *)aSet;
+ (BCSequence *)objectForSavedRepresentation:(NSString *)sequence;
- (NSString *)convertThreeLetterStringToOneLetterString:(NSString *)aString symbolSet: (BCSymbolSet *)aSet;

- (BCSequenceType)sequenceTypeForString:(NSString *)string;
- (BCSequenceType)sequenceTypeForData:(NSData *)aData;
- (BCSequenceType)sequenceTypeForSymbolArray:(NSArray *)anArray;

- (NSData *) sequenceData;
- (const unsigned char *) bytes;
- (NSString*)sequenceString;

- (void)removeSymbolsInRange:(NSRange)aRange;
- (void)removeSymbolAtIndex:(int)index;
- (void)insertSymbolsFromSequence:(BCSequence *)seq atIndex:(int)index;

- (BCSequence *) reverse;
- (BCSequence *) complement;
- (BCSequence *) reverseComplement;


@end
