//
//  EntrezResult.m
//  BioCocoa
//
//  Created by Alexander Griekspoor
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


/* This class encapsulates a result retrieved from Entrez using the controller class 
   It makes life very easy to display results in a tableview */

#import "EntrezResult.h"

#define kVersionNumber 1

@implementation EntrezResult

//===========================================================================
//  Init & Dealloc
//===========================================================================

- (id)initWithID: (int)value;
{
    if (self = [super init]){
        [self setDb_id: value];
    }
    
    return self;
}


- (id)initWithCoder: (NSCoder*) coder
{
    self = [super init];
    
    [self setAccession:  [coder decodeObject]];		// restore name
    [self setDescription:  [coder decodeObject]];       // restore type
    [self setSpecies: [coder decodeObject]];		// restore query
    [self setExtra: [coder decodeObject]];		// restore query

    [coder decodeValueOfObjCType: "i" at: &db_id];		

    return self;
}


- (void)dealloc				// Object deallocation
{
    [accession release];
    [description release];
    [species release];
    [extra release];
    
    [super dealloc];
}


//===========================================================================
//  Archiving and Unarchiving
//===========================================================================

- (void)encodeWithCoder: (NSCoder *) coder
{
    [coder encodeObject:accession];
    [coder encodeObject:description];
    [coder encodeObject:species];
    [coder encodeObject:extra];
    
    [coder encodeValueOfObjCType: "i" at: &db_id];		// store state of logging

}


//===========================================================================
//  Accessor methods
//===========================================================================


- (NSString *)accession
{
	return accession;
}

- (void)setAccession:(NSString *)newAccession
{
	[newAccession retain];
	[accession release];
	accession = newAccession;
}

- (NSString *)description
{
	return description;
}

- (void)setDescription:(NSString *)newDescription
{
	[newDescription retain];
	[description release];
	description = newDescription;
}

- (NSString *)species
{
	return species;
}

- (void)setSpecies:(NSString *)newSpecies
{
	[newSpecies retain];
	[species release];
	species = newSpecies;
}

- (int)db_id
{
	return db_id;
}

- (void)setDb_id:(int)newDb_id
{
	db_id = newDb_id;
}

- (NSString *)extra
{
	return extra;
}

- (void)setExtra:(NSString *)newExtra
{
	[newExtra retain];
	[extra release];
	extra = newExtra;
}


//===========================================================================
//  General methods
//===========================================================================




//===========================================================================
//  Sorting
//===========================================================================

- (NSComparisonResult)sortResultsOnIdAscending: (EntrezResult*) aResult{
    
    if([self db_id] > [aResult db_id]){
        return NSOrderedAscending;
    } else if([self db_id] < [aResult db_id]){
        return NSOrderedDescending;
    } else return NSOrderedSame;
}

- (NSComparisonResult)sortResultsOnIdDescending:(EntrezResult*) aResult{
    
    if([self db_id] < [aResult db_id]){
        return NSOrderedAscending;
    } else if([self db_id] > [aResult db_id]){
        return NSOrderedDescending;
    } else return NSOrderedSame;
}

- (NSComparisonResult)sortResultsOnAccessionAscending: (EntrezResult*) aResult{

    return [[self accession] caseInsensitiveCompare: [aResult accession]];
}

- (NSComparisonResult)sortResultsOnAccessionDescending:(EntrezResult*) aResult{

    return [[aResult accession] caseInsensitiveCompare: [self accession]];
}


- (NSComparisonResult)sortResultsOnDescriptionAscending: (EntrezResult*) aResult{
    
    return [[self description] caseInsensitiveCompare: [aResult description]];
}

- (NSComparisonResult)sortResultsOnDescriptionDescending:(EntrezResult*) aResult{
    
    return [[aResult description] caseInsensitiveCompare: [self description]];
}


- (NSComparisonResult)sortResultsOnSpeciesAscending: (EntrezResult*) aResult{
    
    return [[self species] caseInsensitiveCompare: [aResult species]];
}

- (NSComparisonResult)sortResultsOnSpeciesDescending:(EntrezResult*) aResult{
    
    return [[aResult species] caseInsensitiveCompare: [self species]];
}

@end
