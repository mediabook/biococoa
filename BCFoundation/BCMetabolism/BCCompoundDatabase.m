//
//  BCCompoundDatabase.m
//  BioCocoa
//
//  Created by Scott Christley on 2/15/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BCCompoundDatabase.h"
#import "BCPreferences.h"

static BCCompoundDatabase *defaultCompoundDatabase = nil;

NSString * const BCMetabolismDirectory = @"Metabolism";
NSString * const BCCompoundDatabaseFile = @"CompoundDatabase.txt";

@implementation BCCompoundDatabase

+ (BCCompoundDatabase *)defaultCompoundDatabase
{
  if (!defaultCompoundDatabase) {
    NSString *filePath = [[BCPreferences sharedDataSubdirectory: BCMetabolismDirectory]
                          stringByAppendingPathComponent: BCCompoundDatabaseFile];
    defaultCompoundDatabase = [BCCompoundDatabase new];
    id d = [defaultCompoundDatabase loadDatabaseFromFile: filePath];
    if (!d) {
      [defaultCompoundDatabase release];
      defaultCompoundDatabase = nil;
    }
  }
  
  return defaultCompoundDatabase;
}

- (void)dealloc
{
  if (compoundDatabase) [compoundDatabase release];
  [super dealloc];
}

- (NSDictionary *)database { return compoundDatabase; }

- (NSDictionary *)loadDatabaseFromFile: (NSString *)fileName
{
  compoundDatabase = [NSMutableDictionary new];
  NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
  
  // parse compound database file
  NSError *err = nil;
  NSString *s = [NSString stringWithContentsOfFile: fileName encoding: NSUTF8StringEncoding error: &err];
	if (!s) {
    // try different encoding
    s = [NSString stringWithContentsOfFile: fileName encoding: NSISOLatin1StringEncoding error: &err];
	}
  if (!s) {
		printf("Could not open file: %s\n", [fileName UTF8String]);
		printf("%s\n", [[err localizedDescription] UTF8String]);
		return nil;
	} else printf("Loading compound database: %s\n", [fileName UTF8String]);
  
  NSArray *lines = [s componentsSeparatedByString: @"\n"];
  NSArray *keys = [[lines objectAtIndex: 0] componentsSeparatedByString: @"\t"];
  
  int i, j;
  for (i = 1; i < [lines count]; ++i) {
    if ([[lines objectAtIndex: i] length] == 0) continue;
    
    NSArray *a = [[lines objectAtIndex: i] componentsSeparatedByString: @"\t"];
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    for (j = 0; j < [keys count]; ++j)
      [d setObject: [a objectAtIndex: j] forKey: [keys objectAtIndex: j]];
    [compoundDatabase setObject: d forKey: [a objectAtIndex: 0]];
    //printf("%s\n", [[d description] UTF8String]);
  }
  
  [pool drain];
  return compoundDatabase;
}

@end
