//
//  BCMetabolicModel.m
//  BioCocoa
//
//  Created by Scott Christley on 2/15/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BCMetabolicModel.h"


@implementation BCMetabolicModel

- init
{
  [super init];
  
  lowerBound = -100.0;
  upperBound = 100.0;
  
  return self;
}

- (void)dealloc
{
  if (model) [model release];
  [super dealloc];
}

- (NSDictionary *)metabolicModel { return model; }

- (void)setUpperBound: (double)aBound { upperBound = aBound; }
- (double)upperBound { return upperBound; }
- (void)setLowerBound: (double)aBound { lowerBound = aBound; }
- (double)lowerBound { return lowerBound; }
- (void)setBiomassReaction: (NSString *)aReaction { biomassReaction = aReaction; }
- (NSString *)biomassReaction { return biomassReaction; }

- (NSMutableDictionary *)geneSet
{
  int i, j, k;
  NSMutableDictionary *geneSet = [NSMutableDictionary dictionary];
  
  NSArray *modelRules = [model objectForKey: @"REACTIONS"];
  for (i = 0; i < [modelRules count]; ++i) {
    NSDictionary *d = [modelRules objectAtIndex: i];
    NSArray *pegs = [d objectForKey: @"PEGS"];
    for (j = 0; j < [pegs count]; ++j) {
      NSArray *g = [pegs objectAtIndex: j];
      for (k = 0; k < [g count]; ++k) {
        NSMutableSet *a = [geneSet objectForKey: [g objectAtIndex: k]];
        if (!a) {
          a = [NSMutableSet set];
          [geneSet setObject: a forKey: [g objectAtIndex: k]];
        }
        [a addObject: [d objectForKey: @"DATABASE"]];
      }
    }
  }

  return geneSet;
}

- (NSDictionary *)loadModelFromFile:(NSString *)fileName withReactions: (NSDictionary *)reactionDatabase
                         andBiomass:(NSDictionary *)biomassDatabase
{
  model = [NSMutableDictionary dictionary];
  NSMutableDictionary *modelReactions = [NSMutableDictionary dictionary];
  NSMutableArray *modelRules = [NSMutableArray array];
  NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
  
  [model setObject: modelRules forKey: @"REACTIONS"];
  
  // parse model file
  NSError *err = nil;
  NSString *s = [NSString stringWithContentsOfFile: fileName encoding: NSUTF8StringEncoding error: &err];
	if (!s) {
    // try different encoding
    s = [NSString stringWithContentsOfFile: fileName encoding: NSISOLatin1StringEncoding error: &err];
	}
  if (!s) {
		printf("Could not open file: %s\n", [fileName UTF8String]);
		printf("%s\n", [[err localizedDescription] UTF8String]);
		return nil;
	} else printf("Loading model: %s\n", [fileName UTF8String]);
  
  // first line is header
  NSArray *lines = [s componentsSeparatedByString: @"\n"];
  NSArray *keys = [[lines objectAtIndex: 0] componentsSeparatedByString: @"\t"];
  printf("lines = %lu\n", (unsigned long)[lines count]);
  printf("keys = %lu\n", (unsigned long)[keys count]);
  //printf("%s\n", [[keys description] UTF8String]);
  
  int i, j, k;
  for (i = 1; i < [lines count]; ++i) {
    if ([[lines objectAtIndex: i] length] == 0) continue;
    
    NSArray *a = [[lines objectAtIndex: i] componentsSeparatedByString: @"\t"];
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    if ([keys count] != [a count])
      printf("bad line (%d): %s\n", i, [[lines objectAtIndex: i] UTF8String]);
    else {
      for (j = 0; j < [keys count]; ++j) {
        if ([[keys objectAtIndex: j] isEqualToString: @"PEGS"]) {
          // parse gene set
          NSMutableArray *geneSet = [NSMutableArray array];
          [d setObject: geneSet forKey: [keys objectAtIndex: j]];
          if ([[a objectAtIndex:j] isEqualToString: @""]) continue;
          NSArray *g1 = [[a objectAtIndex: j] componentsSeparatedByString: @"|"];
          for (k = 0; k < [g1 count]; ++k) {
            NSArray *g2 = [[g1 objectAtIndex: k] componentsSeparatedByString: @"+"];
            [geneSet addObject: g2];
          }
        } else
          [d setObject: [a objectAtIndex: j] forKey: [keys objectAtIndex: j]];
      }
      
      NSString *s = [d objectForKey: @"EQUATION"];
      NSRange r = [s rangeOfString: @"<=>"];
      if (r.location != NSNotFound) {
        [d setObject: [NSNumber numberWithDouble: lowerBound] forKey: @"LOWER"];
        [d setObject: [NSNumber numberWithDouble: upperBound] forKey: @"UPPER"];
      } else {
        r = [s rangeOfString: @"=>"];
        if (r.location != NSNotFound) {
          [d setObject: [NSNumber numberWithDouble: 0] forKey: @"LOWER"];
          [d setObject: [NSNumber numberWithDouble: upperBound] forKey: @"UPPER"];
        } else {
          r = [s rangeOfString: @"<="];
          if (r.location != NSNotFound) {
            [d setObject: [NSNumber numberWithDouble: lowerBound] forKey: @"LOWER"];
            [d setObject: [NSNumber numberWithDouble: 0] forKey: @"UPPER"];
          } else {
            printf("Unknown directionality: %s line (%d): %s\n", [s UTF8String], i, [[lines objectAtIndex: i] UTF8String]);
            printf("Assuming reversible.\n");
            [d setObject: [NSNumber numberWithDouble: lowerBound] forKey: @"LOWER"];
            [d setObject: [NSNumber numberWithDouble: upperBound] forKey: @"UPPER"];
          }
        }
      }
      
      if ([modelReactions objectForKey: [a objectAtIndex: 0]]) {
        printf("Reaction already in model: %s\n", [[a objectAtIndex: 0] UTF8String]);
        continue;
      }
      
      id modelRule = [reactionDatabase objectForKey: [a objectAtIndex: 0]];
      if (modelRule) {
        // regular reaction
        [modelReactions setObject: [a objectAtIndex: 0] forKey: [a objectAtIndex: 0]];
      } else {
        modelRule = [biomassDatabase objectForKey: [a objectAtIndex: 0]];
        if (modelRule) {
          // biomass reaction
          [model setObject: [a objectAtIndex: 0] forKey: @"BIOMASS"];
          [modelReactions setObject: [a objectAtIndex: 0] forKey: [a objectAtIndex: 0]];          
        } else {
          printf("Unknown reaction: %s line (%d): %s\n", [[a objectAtIndex: 0] UTF8String], i, [[lines objectAtIndex: i] UTF8String]);
          continue;
        }
      }
      
      [modelRules addObject: d];
      //printf("%s\n", [[d description] UTF8String]);
      
    }
  }
  
  [pool drain];
  return model;
}

@end
