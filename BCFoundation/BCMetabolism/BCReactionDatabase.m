//
//  BCReactionDatabase.m
//  BioCocoa
//
//  Created by Scott Christley on 2/15/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BCReactionDatabase.h"
#import "BCCompoundDatabase.h"
#import "BCPreferences.h"

static BCReactionDatabase *defaultReactionDatabase = nil;

NSString * const BCReactionDatabaseFile = @"ReactionDatabase.txt";

@implementation BCReactionDatabase

+ (BCReactionDatabase *)defaultReactionDatabase
{
  if (!defaultReactionDatabase) {
    NSString *filePath = [[BCPreferences sharedDataSubdirectory: BCMetabolismDirectory]
                          stringByAppendingPathComponent: BCReactionDatabaseFile];
    defaultReactionDatabase = [BCReactionDatabase new];
    id d = [defaultReactionDatabase loadDatabaseFromFile: filePath
                                           withCompounds: [[BCCompoundDatabase defaultCompoundDatabase] database]];
    if (!d) {
      [defaultReactionDatabase release];
      defaultReactionDatabase = nil;
    }
  }
  
  return defaultReactionDatabase;
}

- (void)dealloc
{
  if (reactionDatabase) [reactionDatabase release];
  [super dealloc];
}

- (NSDictionary *)database { return reactionDatabase; }

- (NSDictionary *)loadDatabaseFromFile:(NSString *)fileName withCompounds: (NSDictionary *)compoundDatabase
{
  reactionDatabase = [NSMutableDictionary new];
  NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
  
  // parse reaction database file
  NSError *err = nil;
  NSString *s = [NSString stringWithContentsOfFile: fileName encoding: NSUTF8StringEncoding error: &err];
	if (!s) {
    // try different encoding
    s = [NSString stringWithContentsOfFile: fileName encoding: NSISOLatin1StringEncoding error: &err];
	}
  if (!s) {
		printf("Could not open file: %s\n", [fileName UTF8String]);
		printf("%s\n", [[err localizedDescription] UTF8String]);
		return nil;
	} else printf("Loading reaction database: %s\n", [fileName UTF8String]);
  
  NSArray *lines = [s componentsSeparatedByString: @"\n"];
  NSArray *keys = [[lines objectAtIndex: 0] componentsSeparatedByString: @"\t"];
  
  int i, j, k;
  for (i = 1; i < [lines count]; ++i) {
    if ([[lines objectAtIndex: i] length] == 0) continue;
    
    NSArray *a = [[lines objectAtIndex: i] componentsSeparatedByString: @"\t"];
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    NSString *reactionID = [a objectAtIndex: 0];
    if ([keys count] != [a count]) {
      printf("bad line (%d) incorrect number of entries: %s\n", i, [[lines objectAtIndex: i] UTF8String]);
      continue;
    } else {
      for (j = 0; j < [keys count]; ++j)
        [d setObject: [a objectAtIndex: j] forKey: [keys objectAtIndex: j]];
      
      // parse reaction equation
      NSArray *e = [[a objectAtIndex: 2] componentsSeparatedByString: @" "];
      NSMutableArray *reactants = [NSMutableArray array];
      NSMutableArray *products = [NSMutableArray array];
      double coefficient = -1;
      BOOL parseReactants = YES;
      BOOL isERROR = NO;
      for (k = 0; k < [e count]; ++k) {
        NSString *t = [e objectAtIndex: k];
        if (!t || ([t length] == 0)) {
          printf("Missing equation for reaction: %s\n", [reactionID UTF8String]);
          isERROR = YES;
          break;
        }
        if ([t characterAtIndex: 0] == '(') {
          NSRange r = [t rangeOfString: @")"];
          r.length = r.location - 1; r.location = 1;
          t = [t substringWithRange: r];
          coefficient = [t doubleValue];
          if (parseReactants) coefficient = -coefficient;
        } else if ([t characterAtIndex: 0] == '+') {
          continue;
        } else if ([t isEqualToString: @"<=>"] || [t isEqualToString: @"=>"] || [t isEqualToString: @"<="]) {
          parseReactants = NO;
          coefficient = 1;
        } else {
          NSMutableDictionary *ed = [NSMutableDictionary dictionary];
          
          // determine if in another compartment
          NSRange r = [t rangeOfString: @"["];
          if (r.location != NSNotFound) {
            NSRange r2 = [t rangeOfString: @"]"];
            r.location += 1; r.length = r2.location - r.location;
            NSString *compartment = [t substringWithRange: r];
            [ed setObject: compartment forKey: @"COMPARTMENT"];
            r.length = r.location - 1; r.location = 0;
            t = [t substringWithRange: r];
          }
          
          NSDictionary *compound = [compoundDatabase objectForKey: t];
          if (!compound) printf("Compound not found for reaction (%s) in database: %s\n", [reactionID UTF8String], [t UTF8String]);
          
          [ed setObject: t forKey: @"DATABASE"];
          [ed setObject: [NSNumber numberWithDouble: coefficient] forKey: @"COEFFICIENT"];
          if (parseReactants) {
            [reactants addObject: ed];
            coefficient = -1;
          } else {
            [products addObject: ed];
            coefficient = 1;
          }
        }
      }
      [d setObject: reactants forKey: @"REACTANTS"];
      [d setObject: products forKey: @"PRODUCTS"];

      // made it through all the checks so add reaction
      if (!isERROR) [reactionDatabase setObject: d forKey: reactionID];

      //printf("%s\n", [[d description] UTF8String]);
      //printf("%s\n", [[reactants description] UTF8String]);
      //printf("%s\n", [[products description] UTF8String]);
    }
  }
  
  [pool drain];
  return reactionDatabase;
}

@end
