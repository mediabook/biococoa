//
//  BCMetabolicModel.h
//  BioCocoa
//
//  Created by Scott Christley on 2/15/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <Foundation/Foundation.h>

/*!
 @header
 @abstract A specific metabolic model.
 */

/*!
 @class      BCMetabolicModel
 @abstract   A specific metabolic model.
 @discussion This class provides a wrapper around a ModelSEED metabolic
 model which consists of a set of reaction rules, directionality of the
 reaction and the gene set for the reaction. A single metabolic model
 is usually for a single organism but that is not a requirement.
 */

@interface BCMetabolicModel : NSObject {
  NSMutableDictionary *model;
  
  double upperBound;
  double lowerBound;
  NSString *biomassReaction;
}

- (NSDictionary *)metabolicModel;
- (void)setUpperBound: (double)aBound;
- (double)upperBound;
- (void)setLowerBound: (double)aBound;
- (double)lowerBound;
- (void)setBiomassReaction: (NSString *)aReaction;
- (NSString *)biomassReaction;

- (NSMutableDictionary *)geneSet;

/*!
 @method     loadModelFromFile:withReactions:andBiomass:
 @abstract   Loads a metabolic model from the given file using the given reaction and biomass database.
 @discussion	This method loads a set of reactions rules from a
 text file that is in ModelSEED format. This format is a tab delimited
 format with the first line being the header which become the keys
 in the NSDictionary and each reaction rule on a single line. An example
 header line looks like this:
 
 <pre>
 @textblock
 DATABASE    EQUATION    ABBREVIATION EQ    PEGS
 @/textblock
 </pre>

 with an example reaction rule:
 
 <pre>
 @textblock
 rxn12008    cpd00012 + cpd02557 <= cpd00113 + cpd02590    ppi + octdp <= ipdp + hepdp    peg.4042
 @/textblock
 </pre>
 
 The fields are:
 
 <ul>
 <li>DATABASE - Unique identifier for reaction, this should match the reaction in the reaction database
 <li>EQUATION - The reaction rule with compounds and their stochiometry
 <li>ABBREVIATION EQ - The reaction rule using abbreviated names for the compounds
 <li>PEGS - identifiers for the gene set involved with the reaction.
 </ul>

 The gene set is provided as a logical AND/OR structure with OR signified by
 the vertical bar '|' and AND signified by the plus symbol '+'. The following
 example gene set:
 
 <pre>
 @textblock
 peg.1810|peg.4496+peg.4497|peg.4500+peg.4502+peg.4503+peg.4504+peg.4505+peg.4506|peg.5314
 @/textblock
 </pre>

 represents that the reaction can be catalyzed according the boolean expression:
 
 peg.1810 OR (peg.4496 AND peg.4497) OR (peg.4500 AND peg.4502 AND peg.4503 AND peg.4504 AND peg.4505 AND peg.4506) OR peg.5314
 
 How this expression is interepreted is context dependent. The genes may be enzymes, but they could also
 represent a receptor complex when the reaction is a membrane transport function. The gene set is
 parsed into a property list as an NSArray of NSArrays with the top level NSArray for the OR's and the
 second level NSArray listing each gene for the AND's. The above expression is represented by the property list:
 
 <pre>
 @textblock
 (
   (peg.1810),
   (peg.4496, peg.4497),
   (peg.4500, peg.4502, peg.4503, peg.4504, peg.4505, peg.4506),
   (peg.5313)
 )
 @/textblock
 </pre>

 
 */
- (NSDictionary *)loadModelFromFile:(NSString *)fileName withReactions: (NSDictionary *)reactionDatabase
                         andBiomass:(NSDictionary *)biomassDatabase;


@end
