//
//  BCReactionDatabase.h
//  BioCocoa
//
//  Created by Scott Christley on 2/15/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <Foundation/Foundation.h>

/*!
 @header
 @abstract Reaction database for metabolic and other models. 
 */

/*!
 @class      BCReactionDatabase
 @abstract   Reaction database for metabolic and other models.
 @discussion This class provides a wrapper around a ModelSEED reaction database.
 */

@interface BCReactionDatabase : NSObject {
  NSMutableDictionary *reactionDatabase;
}

+ (BCReactionDatabase *)defaultReactionDatabase;

- (NSDictionary *)database;

/*!
 @method     loadDatabaseFromFile:withCompounds:
 @abstract   Loads a reaction database from the given file using the given compound database.
 @discussion	This method loads a set of reactions rules from a
 text file that is in ModelSEED format. This format is a comma delimited
 format with the first line being the header which become the keys
 in the NSDictionary and each reaction rule on a single line. An example
 header line looks like this:

 <pre>
 @textblock
 DATABASE;NAME;EQUATION;CODE;MAIN EQUATION;ENZYME;PATHWAY;KEGG MAPS;REVERSIBILITY;DELTAG;DELTAGERR;KEGGID;ARGONNEID;MODELID;MODELS
 @/textblock
 </pre>

 with an example reaction rule:
 
 <pre>
 @textblock
 rxn00001;Pyrophosphate phosphohydrolase|inorganic diphosphatase|inorganic diphosphatase, mitochondrial|Pyrophosphate phosphohydrolase|PPA|inorganic diphosphatase (one proton translocation)|inorganicdiphosphatase|inorganic pyrophosphatase;cpd00001 + cpd00012 <=> (2) cpd00009 + (2) cpd00067;cpd00001 + cpd00012 <=> (2) cpd00009;;3.6.1.1;Anaplerotic Reactions|Oxidative phosphorylation|Phosphate and sulfur|Anaplerotic reactions|Central Metabolism|Miscellaneous|Transport|Other|Oxidative Phosphorylation;;=>;-5.391;1.22474;R00004;rxn00001|rxn10590;rbs_267|INORGPYROPHOSPHAT-RXN|R00004|PPA|PPA_1|rbs_266|rxn10590|PPAm|IPP1;All:rbs_267|All:INORGPYROPHOSPHAT-RXN|All:R00004|All:PPA|All:PPA_1|All:rbs_266|All:rxn10590|All:PPAm|iAbaylyiv4:INORGPYROPHOSPHAT-RXN|iAF1260:PPA|iAF692:PPA_1|iAF692:PPA|iAG612:rbs_266|iAG612:rbs_267|iIN800:IPP1|iIT341:PPA|iJN746:PPA|iJR904:PPA|iMEO21:PPA|iMM904:PPA|iMM904:PPAm|iND750:PPA|iND750:PPAm|iNJ661:PPA|iPS189:PPA|iSB619:PPA|iSO783:PPA|iYO844:PPA
 @/textblock
 </pre>

 Some fields allow for multiple value which are separated by the
 vertical bar '|'.
 
 The most typical fields are:
 
 <ul>
 <li>DATABASE - Unique identifier for reaction
 <li>NAME - Common names for reaction
 <li>EQUATION - The reaction rule with compounds and their stochiometry
 </ul>
 */

- (NSDictionary *)loadDatabaseFromFile:(NSString *)fileName withCompounds: (NSDictionary *)compoundDatabase;

@end
