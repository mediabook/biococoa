//
//  BCMediaDatabase.m
//  BioCocoa
//
//  Created by Scott Christley on 2/15/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//

#import "BCMediaDatabase.h"


@implementation BCMediaDatabase

- (void)dealloc
{
  if (media) [media release];
  [super dealloc];
}

- (NSDictionary *)database { return media; }

- (NSDictionary *)loadDatabaseFromFile:(NSString *)fileName withCompounds: (NSDictionary *)compoundDatabase
{
  media = [NSMutableDictionary dictionary];
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  
  // parse reaction database file
  NSError *err = nil;
  NSString *s = [NSString stringWithContentsOfFile: fileName encoding: NSUTF8StringEncoding error: &err];
	if (!s) {
    // try different encoding
    s = [NSString stringWithContentsOfFile: fileName encoding: NSISOLatin1StringEncoding error: &err];
	}
  if (!s) {
		printf("Could not open file: %s\n", [fileName UTF8String]);
		printf("%s\n", [[err localizedDescription] UTF8String]);
		return nil;
	} else printf("Loading media: %s\n", [fileName UTF8String]);
  
  NSArray *lines = [s componentsSeparatedByString: @"\n"];
  NSArray *keys = [[lines objectAtIndex: 0] componentsSeparatedByString: @";"];
  
  int i, j, k;
  for (i = 1; i < [lines count]; ++i) {
    if ([[lines objectAtIndex: i] length] == 0) continue;
    
    NSArray *a = [[lines objectAtIndex: i] componentsSeparatedByString: @";"];
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    if ([keys count] != [a count])
      printf("bad line (%d): %s\n", i, [[lines objectAtIndex: i] UTF8String]);
    else {
      [d setObject: [a objectAtIndex: 0] forKey: @"DATABASE"];
      [d setObject: [a objectAtIndex: 1] forKey: @"TYPE"];
      [d setObject: [a objectAtIndex: 2] forKey: @"COMPARTMENT"];
      [d setObject: [a objectAtIndex: 3] forKey: @"LOWER"];
      [d setObject: [a objectAtIndex: 4] forKey: @"UPPER"];
      [media setObject: d forKey: [a objectAtIndex: 0]];
    }
  } 
  
  //printf("%s\n", [[media description] UTF8String]);
  
  [pool drain];
  return media;
}

@end
