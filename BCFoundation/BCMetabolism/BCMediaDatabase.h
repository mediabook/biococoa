//
//  BCMediaDatabase.h
//  BioCocoa
//
//  Created by Scott Christley on 2/15/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BCMediaDatabase : NSObject {
  NSMutableDictionary *media;
}

- (NSDictionary *)database;

- (NSDictionary *)loadDatabaseFromFile:(NSString *)fileName withCompounds: (NSDictionary *)compoundDatabase;

@end
