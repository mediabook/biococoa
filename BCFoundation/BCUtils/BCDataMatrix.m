//
//  BCDataMatrix.m
//  BioCocoa
//
//  Created by Scott Christley on 7/25/08.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BCDataMatrix.h"
#import "BCInternal.h"

#if 0
#ifdef GNUSTEP
// GNU Scientific Library
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#else
// Accelerate framework on Mac
#import <Accelerate/Accelerate.h>
#endif
#endif

#if 0
#pragma mark == ENCODING STRINGS ==
#endif

char * const BCidEncode = @encode(id);
char * const BCintEncode = @encode(int);
char * const BCdoubleEncode = @encode(double);
char * const BCfloatEncode = @encode(float);
char * const BClongEncode = @encode(long);
char * const BCboolEncode = @encode(BOOL);

#if 0
#pragma mark == FORMAT STRINGS ==
#endif

NSString * const BCParseColumnNames = @"parseColumnNames";
NSString * const BCParseRowNames = @"parseRowNames";
NSString * const BCColumnNames = @"columnNames";
NSString * const BCRowNames = @"rowNames";
NSString * const BCSkipHeaderLines = @"skipHeaderLines";
NSString * const BCDataLayout = @"dataLayout";
NSString * const BCMatrixFormat = @"matrixFormat";
NSString * const BCListFormat = @"listFormat";
NSString * const BCSeparatorCharacterSet = @"separatorCharacterSet";

@implementation BCDataMatrix

+ (BCDataMatrix *)emptyDataMatrixWithRows: (unsigned int)rows andColumns: (unsigned int)cols andEncode: (char *)anEncode
{
	return [[[self alloc] initEmptyDataMatrixWithRows: rows andColumns: cols andEncode: anEncode] autorelease];
}

+ (BCDataMatrix *)dataMatrixWithContentsOfFile: (NSString *)aFile andEncode: (char *)anEncode
{
	return [[[self alloc] initWithContentsOfFile: aFile andEncode: anEncode] autorelease];
}

+ (BCDataMatrix *)dataMatrixWithContentsOfFile: (NSString *)aFile andEncode: (char *)anEncode andFormat: (NSDictionary *)format
{
	return [[[self alloc] initWithContentsOfFile: aFile andEncode: anEncode andFormat: format] autorelease];
}

+ (BCDataMatrix *)dataMatrixWithDataMatrix: (BCDataMatrix *)aMatrix
{
  return [[[self alloc] initDataMatrixWithDataMatrix: aMatrix] autorelease];
}

+ (BCDataMatrix *)dataMatrixWithDataMatrix: (BCDataMatrix *)aMatrix andEncode: (char *)anEncode
{
  return [[[self alloc] initDataMatrixWithDataMatrix: aMatrix andEncode: anEncode] autorelease];
}

+ (BCDataMatrix *)dataMatrixWithDataMatrix: (BCDataMatrix *)aMatrix andEncode: (char *)anEncode isColumnMajor: (BOOL)aFlag
{
  return [[[self alloc] initDataMatrixWithDataMatrix: aMatrix andEncode: anEncode isColumnMajor: aFlag] autorelease];
}

- (BCDataMatrix *)initEmptyDataMatrixWithRows: (unsigned int)rows andColumns: (unsigned int)cols andEncode: (char *)anEncode
{
  return [self initEmptyDataMatrixWithRows: rows andColumns: cols andEncode: anEncode isColumnMajor: NO];
}

- (BCDataMatrix *)initEmptyDataMatrixWithRows: (unsigned int)rows andColumns: (unsigned int)cols andEncode: (char *)anEncode isColumnMajor: (BOOL)aFlag
{
	[super init];
	
	numOfRows = rows;
	numOfCols = cols;
	encode = anEncode;
	isColumnMajor = aFlag;
	
	int i, j;
	if (!strcmp(encode, BCidEncode)) {
		dataMatrix = malloc(sizeof(id) * numOfRows * numOfCols);
		id (*grid)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid)[i][j] = nil;
	} else if (!strcmp(encode, BCintEncode)) {
		dataMatrix = malloc(sizeof(int) * numOfRows * numOfCols);
		int (*grid)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid)[i][j] = 0;
	} else if (!strcmp(encode, BClongEncode)) {
		dataMatrix = malloc(sizeof(long) * numOfRows * numOfCols);
		long (*grid)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid)[i][j] = 0;
	} else if (!strcmp(encode, BCfloatEncode)) {
		dataMatrix = malloc(sizeof(float) * numOfRows * numOfCols);
		float (*grid)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid)[i][j] = 0.0;
	} else if (!strcmp(encode, BCdoubleEncode)) {
		dataMatrix = malloc(sizeof(double) * numOfRows * numOfCols);
		double (*grid)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid)[i][j] = 0.0;
	} else if (!strcmp(encode, BCboolEncode)) {
		dataMatrix = malloc(sizeof(BOOL) * numOfRows * numOfCols);
		BOOL (*grid)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid)[i][j] = NO;
	} else {
		// throw exception or something
		NSLog(@"ERROR: BCDataMatrix unknown encoding %s\n", anEncode);
		return nil;
	}
	
	return self;
}

- (BCDataMatrix *)initWithContentsOfFile: (NSString *)aFile andEncode: (char *)anEncode
{
	return [self initWithContentsOfFile: aFile andEncode: anEncode andFormat: nil];
}

- (BCDataMatrix *)initWithContentsOfFile: (NSString *)aFile andEncode: (char *)anEncode andFormat: (NSDictionary *)format
{
	BOOL parseRowNames, parseColNames, matrixLayout;
	NSCharacterSet *separator;

	// default format is no row/column names and matrix layout
	parseRowNames = NO;
	parseColNames = NO;
	matrixLayout = YES;
	separator = [NSCharacterSet whitespaceCharacterSet];

	if (format) {
		NSString *s = [format objectForKey: BCDataLayout];
		if ([s isEqualToString: BCListFormat]) matrixLayout = NO;
		if (matrixLayout) {
			// we can parse the row/columns in matrix layout
			s = [format objectForKey: BCParseColumnNames];
			if (s && [s boolValue]) parseColNames = YES;
			s = [format objectForKey: BCParseRowNames];
			if (s && [s boolValue]) parseRowNames = YES;
		} else {
			// row/columns names must be given to us for list layout
			colNames = (NSArray *)[format objectForKey: BCColumnNames];
			if (!colNames) {
				NSLog(@"ERROR: List of column names required for list layout");
				return nil;
			}
			colNames = [[NSArray alloc] initWithArray: colNames];
			numOfCols = [colNames count];
			rowNames = (NSArray *)[format objectForKey: BCRowNames];
			if (!rowNames) {
				NSLog(@"ERROR: List of row names required for list layout");
				return nil;
			}
			rowNames = [[NSArray alloc] initWithArray: rowNames];
			numOfRows = [rowNames count];
		}
		s = [format objectForKey: BCSeparatorCharacterSet];
		if (s) separator = [NSCharacterSet characterSetWithCharactersInString: s];
	}

  int skipHeaderLines = [[format objectForKey: BCSkipHeaderLines] intValue];
  int skippingLines = skipHeaderLines;

  NSStringEncoding enc;
  NSError *error;
	NSString *contents = [NSString stringWithContentsOfFile: aFile usedEncoding: &enc error: &error];
	if (!contents) {
		NSLog(@"ERROR: Unable to read contents of file: %@", aFile);
		return nil;
	}

	if (matrixLayout) {
		NSUInteger start, end, next; 
		NSUInteger i, j;
		NSAutoreleasePool *pool = [NSAutoreleasePool new];
		NSRange range;
		unsigned stringLength = [contents length];
		BOOL parseFailure = NO;

		// scan through it once to determine number of rows and columns
		range.location = 0;
		range.length = 1;
		i = 0; j = 0;
		do {
			// one line at a time
			[contents getLineStart:&start end:&next contentsEnd:&end forRange:range]; 
			range.location = start; 
			range.length = end-start; 
			NSString *s = [contents substringWithRange: range];

			// skip empty lines
			if ([s length] == 0) {
				range.location = next; 
				range.length = 1; 
				continue;
			}

      // skip any header lines
      if (skippingLines) {
        --skippingLines;
				range.location = next; 
				range.length = 1; 
        continue;
      }
      
			NSArray *a = [s componentsSeparatedByCharactersInSet: separator];

			if (i == 0) {
				if (parseColNames) {
          NSRange r = NSMakeRange(0, [a count]);
          if (parseRowNames) {
            ++r.location;
            --r.length;
          }
          colNames = [[a subarrayWithRange: r] retain];
					numOfCols = [colNames count];
				} else {
					if (parseRowNames) {
						numOfCols = [a count] - 1;
					} else {
						numOfCols = [a count];
					}
				}
			}
			++i;

			range.location = next; 
			range.length = 1; 
		} while (next < stringLength);

		int expectCols = numOfCols;
		if (parseRowNames) {
			rowNames = [NSMutableArray new];
			expectCols = numOfCols + 1;
		}
		numOfRows = i;
		if (parseColNames) --numOfRows;

		[self initEmptyDataMatrixWithRows: numOfRows andColumns: numOfCols andEncode: anEncode];

    skippingLines = skipHeaderLines;

		// scan through again to parse the data
		range.location = 0;
		range.length = 1;
		i = 0; j = 0;
		do {
			// one line at a time
			[contents getLineStart:&start end:&next contentsEnd:&end forRange:range]; 
			range.location = start; 
			range.length = end-start; 
			NSString *s = [contents substringWithRange: range];

			// skip empty lines
			if ([s length] == 0) {
				range.location = next; 
				range.length = 1; 
				continue;
			}

      // skip any header lines
      if (skippingLines) {
        --skippingLines;
				range.location = next; 
				range.length = 1; 
        continue;
      }
      
			if ((i == 0) && (parseColNames)) {
				// skip the column names
				parseColNames = NO;
			} else {
				NSArray *a = [s componentsSeparatedByCharactersInSet: separator];
				if ([a count] != expectCols) {
					NSLog(@"Invalid matrix format for data matrix, expected %d items, got %lu", expectCols, [a count]);
					NSLog(@"Offending line:");
					NSLog(@"%@", s);
					parseFailure = YES;
					break;
				}

				if (parseRowNames) [(NSMutableArray *)rowNames addObject: [a objectAtIndex: 0]];

				if (!strcmp(encode, BCidEncode)) {
					id (*grid)[numOfRows][numOfCols] = dataMatrix;
					for (j = 0; j < numOfCols; ++j)
						if (parseRowNames) (*grid)[i][j] = [a objectAtIndex: (j + 1)];
						else (*grid)[i][j] = [a objectAtIndex: j];
				} else if (!strcmp(encode, BCintEncode)) {
					int (*grid)[numOfRows][numOfCols] = dataMatrix;
					for (j = 0; j < numOfCols; ++j)
						if (parseRowNames) (*grid)[i][j] = [[a objectAtIndex: (j + 1)] intValue];
						else (*grid)[i][j] = [[a objectAtIndex: j] intValue];
				} else if (!strcmp(encode, BClongEncode)) {
					long (*grid)[numOfRows][numOfCols] = dataMatrix;
					for (j = 0; j < numOfCols; ++j)
						if (parseRowNames) (*grid)[i][j] = [[a objectAtIndex: (j + 1)] longValue];
						else (*grid)[i][j] = [[a objectAtIndex: j] longValue];
				} else if (!strcmp(encode, BCfloatEncode)) {
					float (*grid)[numOfRows][numOfCols] = dataMatrix;
					for (j = 0; j < numOfCols; ++j)
						if (parseRowNames) (*grid)[i][j] = [[a objectAtIndex: (j + 1)] floatValue];
						else (*grid)[i][j] = [[a objectAtIndex: j] floatValue];
				} else if (!strcmp(encode, BCdoubleEncode)) {
					double (*grid)[numOfRows][numOfCols] = dataMatrix;
					for (j = 0; j < numOfCols; ++j)
						if (parseRowNames) (*grid)[i][j] = [[a objectAtIndex: (j + 1)] doubleValue];
						else (*grid)[i][j] = [[a objectAtIndex: j] doubleValue];
				} else if (!strcmp(encode, BCboolEncode)) {
					BOOL (*grid)[numOfRows][numOfCols] = dataMatrix;
					for (j = 0; j < numOfCols; ++j)
						if (parseRowNames) (*grid)[i][j] = [[a objectAtIndex: (j + 1)] boolValue];
						else (*grid)[i][j] = [[a objectAtIndex: j] boolValue];
				}

				++i;
			}

			range.location = next; 
			range.length = 1; 
		} while (next < stringLength); 

		[pool release];
		if (parseFailure) {
			[self dealloc];
			return nil;
		}

	} else {
		// The list layout has one line for each entry in the format
		//		rowName colName value

		NSUInteger start, end, next; 
		NSUInteger i, j;
		NSAutoreleasePool *pool = [NSAutoreleasePool new];
		NSRange range;
		unsigned stringLength = [contents length];
		BOOL parseFailure = NO;

		[self initEmptyDataMatrixWithRows: numOfRows andColumns: numOfCols andEncode: anEncode];

		range.location = 0;
		range.length = 1;
		do {
			// one line at a time
			[contents getLineStart:&start end:&next contentsEnd:&end forRange:range]; 
			range.location = start; 
			range.length = end-start; 
			NSString *s = [contents substringWithRange: range];

			// skip empty lines
			if ([s length] == 0) {
				range.location = next; 
				range.length = 1; 
				continue;
			}

      // skip any header lines
      if (skippingLines) {
        --skippingLines;
				range.location = next; 
				range.length = 1; 
        continue;
      }
      
      
			NSArray *a = [s componentsSeparatedByCharactersInSet: separator];
			if ([a count] != 3) {
				NSLog(@"Invalid list format for data matrix, expected 3 items, got %lu", [a count]);
				NSLog(@"Offending line:");
				NSLog(@"%@", s);
				parseFailure = YES;
				break;
			}

			i = [rowNames indexOfObject: [a objectAtIndex: 0]];
			if (i == NSNotFound) {
				NSLog(@"Found unknown row name in file: %@", [a objectAtIndex: 0]);
				NSLog(@"Offending line:");
				NSLog(@"%@", s);
				parseFailure = YES;
				break;
			}

			j = [rowNames indexOfObject: [a objectAtIndex: 1]];
			if (j == NSNotFound) {
				NSLog(@"Found unknown row name in file: %@", [a objectAtIndex: 1]);
				NSLog(@"Offending line:");
				NSLog(@"%@", s);
				parseFailure = YES;
				break;
			}

			if (!strcmp(encode, BCidEncode)) {
				id (*grid)[numOfRows][numOfCols] = dataMatrix;
				(*grid)[i][j] = [a objectAtIndex: 2];
			} else if (!strcmp(encode, BCintEncode)) {
				int (*grid)[numOfRows][numOfCols] = dataMatrix;
				(*grid)[i][j] = [[a objectAtIndex: 2] intValue];
			} else if (!strcmp(encode, BClongEncode)) {
				long (*grid)[numOfRows][numOfCols] = dataMatrix;
				(*grid)[i][j] = [[a objectAtIndex: 2] longValue];
			} else if (!strcmp(encode, BCfloatEncode)) {
				float (*grid)[numOfRows][numOfCols] = dataMatrix;
				(*grid)[i][j] = [[a objectAtIndex: 2] floatValue];
			} else if (!strcmp(encode, BCdoubleEncode)) {
				double (*grid)[numOfRows][numOfCols] = dataMatrix;
				(*grid)[i][j] = [[a objectAtIndex: 2] doubleValue];
			} else if (!strcmp(encode, BCboolEncode)) {
				BOOL (*grid)[numOfRows][numOfCols] = dataMatrix;
				(*grid)[i][j] = [[a objectAtIndex: 2] boolValue];
			}
				
			range.location = next; 
			range.length = 1; 
		} while (next < stringLength); 

		[pool release];
		if (parseFailure) {
			[self dealloc];
			return nil;
		}
	}

	return self;
}

- (BCDataMatrix *)initDataMatrixWithDataMatrix: (BCDataMatrix *)aMatrix
{
  return [self initDataMatrixWithDataMatrix: aMatrix andEncode: [aMatrix matrixEncoding] isColumnMajor: [aMatrix isColumnMajor]];
}

- (BCDataMatrix *)initDataMatrixWithDataMatrix: (BCDataMatrix *)aMatrix andEncode: (char *)anEncode
{
  return [self initDataMatrixWithDataMatrix: aMatrix andEncode: anEncode isColumnMajor: [aMatrix isColumnMajor]];
}

- (BCDataMatrix *)initDataMatrixWithDataMatrix: (BCDataMatrix *)aMatrix andEncode: (char *)anEncode isColumnMajor: (BOOL)aFlag
{
  // initialize with proper settings
  [self initEmptyDataMatrixWithRows: [aMatrix numberOfRows] andColumns: [aMatrix numberOfColumns] andEncode: anEncode isColumnMajor: [aMatrix isColumnMajor]];
  
  // copy data from provided data matrix
  int i;
  char *otherEncode = [aMatrix matrixEncoding];
  if (!strcmp(encode, BCidEncode)) {
		id *grid2 = dataMatrix;
    if (!strcmp(otherEncode, BCidEncode)) {
      id *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCintEncode)) {
      int *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        [grid2[i] setIntValue: grid1[i]];
    } else if (!strcmp(otherEncode, BClongEncode)) {
      long *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        [grid2[i] setLongValue: grid1[i]];
    } else if (!strcmp(otherEncode, BCfloatEncode)) {
      float *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        [grid2[i] setFloatValue: grid1[i]];
    } else if (!strcmp(otherEncode, BCdoubleEncode)) {
      double *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        [grid2[i] setDoubleValue: grid1[i]];
    } else if (!strcmp(otherEncode, BCboolEncode)) {
      BOOL *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        [grid2[i] setBoolValue: grid1[i]];
    }
	} else if (!strcmp(encode, BCintEncode)) {
		int *grid2 = dataMatrix;
    if (!strcmp(otherEncode, BCidEncode)) {
      id *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = [grid1[i] intValue];
    } else if (!strcmp(otherEncode, BCintEncode)) {
      int *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BClongEncode)) {
      long *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCfloatEncode)) {
      float *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCdoubleEncode)) {
      double *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCboolEncode)) {
      BOOL *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    }
	} else if (!strcmp(encode, BClongEncode)) {
		long *grid2 = dataMatrix;
    if (!strcmp(otherEncode, BCidEncode)) {
      id *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = [grid1[i] longValue];
    } else if (!strcmp(otherEncode, BCintEncode)) {
      int *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BClongEncode)) {
      long *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCfloatEncode)) {
      float *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCdoubleEncode)) {
      double *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCboolEncode)) {
      BOOL *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    }
	} else if (!strcmp(encode, BCfloatEncode)) {
		float *grid2 = dataMatrix;
    if (!strcmp(otherEncode, BCidEncode)) {
      id *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = [grid1[i] floatValue];
    } else if (!strcmp(otherEncode, BCintEncode)) {
      int *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BClongEncode)) {
      long *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCfloatEncode)) {
      float *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCdoubleEncode)) {
      double *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCboolEncode)) {
      BOOL *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    }
	} else if (!strcmp(encode, BCdoubleEncode)) {
		double *grid2 = dataMatrix;
    if (!strcmp(otherEncode, BCidEncode)) {
      id *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = [grid1[i] doubleValue];
    } else if (!strcmp(otherEncode, BCintEncode)) {
      int *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BClongEncode)) {
      long *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCfloatEncode)) {
      float *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCdoubleEncode)) {
      double *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCboolEncode)) {
      BOOL *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    }
	} else if (!strcmp(encode, BCboolEncode)) {
		BOOL *grid2 = dataMatrix;
    if (!strcmp(otherEncode, BCidEncode)) {
      id *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = [grid1[i] boolValue];
    } else if (!strcmp(otherEncode, BCintEncode)) {
      int *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BClongEncode)) {
      long *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCfloatEncode)) {
      float *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCdoubleEncode)) {
      double *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    } else if (!strcmp(otherEncode, BCboolEncode)) {
      BOOL *grid1 = [aMatrix dataMatrix];
      for (i = 0;i < numOfRows*numOfCols; ++i)
        grid2[i] = grid1[i];
    }
	}
  
  if ([aMatrix columnNames]) [self setColumnNames: [NSArray arrayWithArray: [aMatrix columnNames]]];
  if ([aMatrix rowNames]) [self setRowNames: [NSArray arrayWithArray: [aMatrix rowNames]]];

  // set the data format
  [self setColumnMajor: aFlag];
  
  return self;
}

- (void)dealloc
{
	if (rowNames) [rowNames release];
	if (colNames) [colNames release];
	if (dataMatrix) free(dataMatrix);
	[super dealloc];
}

- (unsigned int)numberOfRows { return numOfRows; }
- (unsigned int)numberOfColumns { return numOfCols; }
- (void *)dataMatrix { return dataMatrix; }
- (char *)matrixEncoding { return encode; }

- (BOOL)isColumnMajor { return isColumnMajor; }
- (void)setColumnMajor: (BOOL)aFlag
{
  int i, j;
  
	if (aFlag == isColumnMajor) return;

  if (!strcmp(encode, BCidEncode)) {
    if (isColumnMajor) {
      id (*tmp)[numOfRows][numOfCols] = malloc(sizeof(id) * numOfRows * numOfCols);
      id (*grid)[numOfCols][numOfRows] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[i][j] = (*grid)[j][i];
      memcpy(grid, tmp, sizeof(id) * numOfRows * numOfCols);
      free(tmp);
    } else {
      id (*tmp)[numOfCols][numOfRows] = malloc(sizeof(id) * numOfRows * numOfCols);
      id (*grid)[numOfRows][numOfCols] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[j][i] = (*grid)[i][j];
      memcpy(grid, tmp, sizeof(id) * numOfRows * numOfCols);
      free(tmp);
    }
	} else if (!strcmp(encode, BCintEncode)) {
    if (isColumnMajor) {
      int (*tmp)[numOfRows][numOfCols] = malloc(sizeof(int) * numOfRows * numOfCols);
      int (*grid)[numOfCols][numOfRows] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[i][j] = (*grid)[j][i];
      memcpy(grid, tmp, sizeof(int) * numOfRows * numOfCols);
      free(tmp);
    } else {
      int (*tmp)[numOfCols][numOfRows] = malloc(sizeof(int) * numOfRows * numOfCols);
      int (*grid)[numOfRows][numOfCols] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[j][i] = (*grid)[i][j];
      memcpy(grid, tmp, sizeof(int) * numOfRows * numOfCols);
      free(tmp);
    }
	} else if (!strcmp(encode, BClongEncode)) {
    if (isColumnMajor) {
      long (*tmp)[numOfRows][numOfCols] = malloc(sizeof(long) * numOfRows * numOfCols);
      long (*grid)[numOfCols][numOfRows] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[i][j] = (*grid)[j][i];
      memcpy(grid, tmp, sizeof(long) * numOfRows * numOfCols);
      free(tmp);
    } else {
      long (*tmp)[numOfCols][numOfRows] = malloc(sizeof(long) * numOfRows * numOfCols);
      long (*grid)[numOfRows][numOfCols] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[j][i] = (*grid)[i][j];
      memcpy(grid, tmp, sizeof(long) * numOfRows * numOfCols);
      free(tmp);
    }
	} else if (!strcmp(encode, BCfloatEncode)) {
    if (isColumnMajor) {
      float (*tmp)[numOfRows][numOfCols] = malloc(sizeof(float) * numOfRows * numOfCols);
      float (*grid)[numOfCols][numOfRows] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[i][j] = (*grid)[j][i];
      memcpy(grid, tmp, sizeof(float) * numOfRows * numOfCols);
      free(tmp);
    } else {
      float (*tmp)[numOfCols][numOfRows] = malloc(sizeof(float) * numOfRows * numOfCols);
      float (*grid)[numOfRows][numOfCols] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[j][i] = (*grid)[i][j];
      memcpy(grid, tmp, sizeof(float) * numOfRows * numOfCols);
      free(tmp);
    }
	} else if (!strcmp(encode, BCdoubleEncode)) {
    if (isColumnMajor) {
      double (*tmp)[numOfRows][numOfCols] = malloc(sizeof(double) * numOfRows * numOfCols);
      double (*grid)[numOfCols][numOfRows] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[i][j] = (*grid)[j][i];
      memcpy(grid, tmp, sizeof(double) * numOfRows * numOfCols);
      free(tmp);
    } else {
      double (*tmp)[numOfCols][numOfRows] = malloc(sizeof(double) * numOfRows * numOfCols);
      double (*grid)[numOfRows][numOfCols] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[j][i] = (*grid)[i][j];
      memcpy(grid, tmp, sizeof(double) * numOfRows * numOfCols);
      free(tmp);
    }
	} else if (!strcmp(encode, BCboolEncode)) {
    if (isColumnMajor) {
      BOOL (*tmp)[numOfRows][numOfCols] = malloc(sizeof(BOOL) * numOfRows * numOfCols);
      BOOL (*grid)[numOfCols][numOfRows] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[i][j] = (*grid)[j][i];
      memcpy(grid, tmp, sizeof(BOOL) * numOfRows * numOfCols);
      free(tmp);
    } else {
      BOOL (*tmp)[numOfCols][numOfRows] = malloc(sizeof(BOOL) * numOfRows * numOfCols);
      BOOL (*grid)[numOfRows][numOfCols] = dataMatrix;
      for (i = 0;i < numOfRows; ++i)
        for (j = 0;j < numOfCols; ++j)
          (*tmp)[j][i] = (*grid)[i][j];
      memcpy(grid, tmp, sizeof(BOOL) * numOfRows * numOfCols);
      free(tmp);
    }
	}
    
	isColumnMajor = aFlag;
}

- (NSArray *)rowNames { return rowNames; }
- (void)setRowNames:(NSArray *)anArray { [rowNames release]; rowNames = [anArray retain]; }
- (NSArray *)columnNames { return colNames; }
- (void)setColumnNames:(NSArray *)anArray { [colNames release]; colNames = [anArray retain]; }

#if 0
#pragma mark == MATRIX OPERATIONS ==
#endif

- (BCDataMatrix *)dataMatrixFromRowRange: (NSRange)rows andColumnRange: (NSRange)cols
{
	// check valid ranges
	if ((rows.length == 0) || (cols.length == 0)) return nil;
	if ((rows.location + rows.length) > numOfRows) return nil;
	if ((cols.location + cols.length) > numOfCols) return nil;

	BCDataMatrix *newMatrix = [BCDataMatrix emptyDataMatrixWithRows: rows.length
		andColumns: cols.length andEncode: encode];

	int i, j;
	if (!strcmp(encode, BCidEncode)) {
		id (*grid1)[numOfRows][numOfCols] = dataMatrix;
		id (*grid2)[rows.length][cols.length] = [newMatrix dataMatrix];
		for (i = 0;i < rows.length; ++i)
			for (j = 0;j < cols.length; ++j)
				(*grid2)[i][j] = (*grid1)[i + rows.location][j + cols.location];
	} else if (!strcmp(encode, BCintEncode)) {
		int (*grid1)[numOfRows][numOfCols] = dataMatrix;
		int (*grid2)[rows.length][cols.length] = [newMatrix dataMatrix];
		for (i = 0;i < rows.length; ++i)
			for (j = 0;j < cols.length; ++j)
				(*grid2)[i][j] = (*grid1)[i + rows.location][j + cols.location];
	} else if (!strcmp(encode, BClongEncode)) {
		long (*grid1)[numOfRows][numOfCols] = dataMatrix;
		long (*grid2)[rows.length][cols.length] = [newMatrix dataMatrix];
		for (i = 0;i < rows.length; ++i)
			for (j = 0;j < cols.length; ++j)
				(*grid2)[i][j] = (*grid1)[i + rows.location][j + cols.location];
	} else if (!strcmp(encode, BCfloatEncode)) {
		float (*grid1)[numOfRows][numOfCols] = dataMatrix;
		float (*grid2)[rows.length][cols.length] = [newMatrix dataMatrix];
		for (i = 0;i < rows.length; ++i)
			for (j = 0;j < cols.length; ++j)
				(*grid2)[i][j] = (*grid1)[i + rows.location][j + cols.location];
	} else if (!strcmp(encode, BCdoubleEncode)) {
		double (*grid1)[numOfRows][numOfCols] = dataMatrix;
		double (*grid2)[rows.length][cols.length] = [newMatrix dataMatrix];
		for (i = 0;i < rows.length; ++i)
			for (j = 0;j < cols.length; ++j)
				(*grid2)[i][j] = (*grid1)[i + rows.location][j + cols.location];
	} else if (!strcmp(encode, BCboolEncode)) {
		BOOL (*grid1)[numOfRows][numOfCols] = dataMatrix;
		BOOL (*grid2)[rows.length][cols.length] = [newMatrix dataMatrix];
		for (i = 0;i < rows.length; ++i)
			for (j = 0;j < cols.length; ++j)
				(*grid2)[i][j] = (*grid1)[i + rows.location][j + cols.location];
	}

	return newMatrix;
}

- (BCDataMatrix *)dataMatrixFromTranspose
{
	BCDataMatrix *newMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfCols
                                                       andColumns: numOfRows andEncode: encode];

  int i, j;
	if (!strcmp(encode, BCidEncode)) {
		id (*grid1)[numOfRows][numOfCols] = dataMatrix;
		id (*grid2)[numOfCols][numOfRows] = [newMatrix dataMatrix];
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid2)[j][i] = (*grid1)[i][j];
	} else if (!strcmp(encode, BCintEncode)) {
		int (*grid1)[numOfRows][numOfCols] = dataMatrix;
		int (*grid2)[numOfCols][numOfRows] = [newMatrix dataMatrix];
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid2)[j][i] = (*grid1)[i][j];
	} else if (!strcmp(encode, BClongEncode)) {
		long (*grid1)[numOfRows][numOfCols] = dataMatrix;
		long (*grid2)[numOfCols][numOfRows] = [newMatrix dataMatrix];
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid2)[j][i] = (*grid1)[i][j];
	} else if (!strcmp(encode, BCfloatEncode)) {
		float (*grid1)[numOfRows][numOfCols] = dataMatrix;
		float (*grid2)[numOfCols][numOfRows] = [newMatrix dataMatrix];
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid2)[j][i] = (*grid1)[i][j];
	} else if (!strcmp(encode, BCdoubleEncode)) {
		double (*grid1)[numOfRows][numOfCols] = dataMatrix;
		double (*grid2)[numOfCols][numOfRows] = [newMatrix dataMatrix];
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid2)[j][i] = (*grid1)[i][j];
	} else if (!strcmp(encode, BCboolEncode)) {
		BOOL (*grid1)[numOfRows][numOfCols] = dataMatrix;
		BOOL (*grid2)[numOfCols][numOfRows] = [newMatrix dataMatrix];
		for (i = 0;i < numOfRows; ++i)
			for (j = 0;j < numOfCols; ++j)
				(*grid2)[j][i] = (*grid1)[i][j];
	}
  
  if (rowNames) [newMatrix setColumnNames: [NSArray arrayWithArray: rowNames]];
  if (colNames) [newMatrix setRowNames: [NSArray arrayWithArray: colNames]];

	return newMatrix;
  
}

- (BOOL)writeToFile: (NSString *)fileName
{
  return [self writeToFile: fileName withFormat: nil];
}

- (BOOL)writeToFile: (NSString *)fileName withFormat: (NSDictionary *)aFormat
{
  BOOL res = NO;

  if (!aFormat) {
    FILE *aFile = fopen([fileName UTF8String], "w");
    if (!aFile) return NO;

    double (*A)[numOfRows][numOfCols] = dataMatrix;
    int i, j;
    
    if (colNames) {
      if (rowNames) fprintf(aFile, "row ");
      for (i = 0; i < [colNames count]; ++i) {
        if (i != 0) fprintf(aFile, " ");
        fprintf(aFile, "%s", [[colNames objectAtIndex: i] UTF8String]);
      }
      fprintf(aFile, "\n");      
    }

    for (i = 0; i < numOfRows; ++i) {
      if (rowNames) fprintf(aFile, "%s ", [[rowNames objectAtIndex: i] UTF8String]);
      fprintf(aFile, "%lf", (*A)[i][0]);
      for (j = 1; j < numOfCols; ++j) {
        fprintf(aFile, " %lf", (*A)[i][j]);
      }
      fprintf(aFile, "\n");
    }
    //fprintf(aFile, "\n");
    
    fclose(aFile);
    res = YES;
  } else {
  }

  return res;
}

- (void)prettyPrintMatrix
{
  int i, j;
	if (!strcmp(encode, BCidEncode)) {
	} else if (!strcmp(encode, BCintEncode)) {
		int (*grid1)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i) {
			for (j = 0;j < numOfCols; ++j) {
        if (j != 0) printf(" ");
				printf("%d", (*grid1)[i][j]);
      }
      printf("\n");
    }
	} else if (!strcmp(encode, BClongEncode)) {
		long (*grid1)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i) {
			for (j = 0;j < numOfCols; ++j) {
        if (j != 0) printf(" ");
				printf("%ld", (*grid1)[i][j]);
      }
      printf("\n");
    }
	} else if (!strcmp(encode, BCfloatEncode)) {
		float (*grid1)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i) {
			for (j = 0;j < numOfCols; ++j) {
        if (j != 0) printf(" ");
				printf("%f", (*grid1)[i][j]);
      }
      printf("\n");
    }
	} else if (!strcmp(encode, BCdoubleEncode)) {
		double (*grid1)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i) {
			for (j = 0;j < numOfCols; ++j) {
        if (j != 0) printf(" ");
				printf("%lf", (*grid1)[i][j]);
      }
      printf("\n");
    }
	} else if (!strcmp(encode, BCboolEncode)) {
		BOOL (*grid1)[numOfRows][numOfCols] = dataMatrix;
		for (i = 0;i < numOfRows; ++i) {
			for (j = 0;j < numOfCols; ++j) {
        if (j != 0) printf(" ");
				if ((*grid1)[i][j]) printf("YES");
        else printf(" NO");
      }
      printf("\n");
    }
	}

}

#if 0
#pragma mark == LINEAR ALGEBRA OPERATIONS ==
#endif

#if 0
- (BCDataMatrix *)LUfactorize
{
#ifdef GNUSTEP
  // create copy of data matrix
  BCDataMatrix *m = [BCDataMatrix dataMatrixWithDataMatrix: self andEncode: BCdoubleEncode isColumnMajor: NO];

  // create a GSL matrix suitable for LU factorize
  gsl_matrix *gm = gsl_matrix_alloc(numOfRows, numOfCols);
  if (!gm) {
    printf("ERROR: Could not allocate GSL matrix.\n");
    return nil;
  }

  int i, j;
  double (*grid)[numOfRows][numOfCols] = [m dataMatrix];
  for (i = 0; i < numOfRows; ++i)
    for (j = 0; j < numOfCols; ++j)
      gsl_matrix_set(gm, i, j, (*grid)[i][j]);

  //gsl_matrix_fprintf(stdout, gm, "%lf");

  int s;
  gsl_permutation *p = gsl_permutation_alloc(numOfRows);
  gsl_linalg_LU_decomp(gm, p, &s);

  //gsl_matrix_fprintf(stdout, gm, "%lf");

  for (i = 0; i < numOfRows; ++i)
    for (j = 0; j < numOfCols; ++j)
      (*grid)[i][j] = gsl_matrix_get(gm, i, j);

  gsl_permutation_free(p);
  gsl_matrix_free(gm);

  return m;

#else

  // create copy of data matrix suitable for LU factorize
  BCDataMatrix *m = [BCDataMatrix dataMatrixWithDataMatrix: self andEncode: BCdoubleEncode isColumnMajor: YES];

  __CLPK_integer M = numOfRows;
  __CLPK_integer N = numOfCols;
  __CLPK_integer INFO;
  __CLPK_integer ipiv[3];
  double *a = [m dataMatrix];

  dgetrf_(&M, &N, a, &M, ipiv, &INFO);
  
  // set result matrix to same data format
  [m setColumnMajor: [self isColumnMajor]];
  
  return m;
#endif
}
#endif

- (double)determinant
{
  if (numOfCols != numOfRows) {
    printf("ERROR: Can only calculate determinant for square matrix.\n");
    return nan("");
  }

  if (!strcmp(encode, BCidEncode)) {
    printf("ERROR: determinant only valid for numerical matrixes.\n");
    return nan("");
	} else if (!strcmp(encode, BCboolEncode)) {
    printf("ERROR: determinant only valid for numerical matrixes.\n");
    return nan("");
	}
  
  BCDataMatrix *m = [self LUfactorize];
  double (*grid)[numOfCols][numOfRows] = [m dataMatrix];
  double det = 1.0;
  int i;
  for (i = 0; i < numOfCols; ++i) {
    det *= (*grid)[i][i];
  }
  
  return det;
}

@end
