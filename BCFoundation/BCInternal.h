//
//  BCInternal.h
//  BioCocoa
//
//  Author: Scott Christley
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


//
// CoreFoundation is significantly faster on Mac than the standard OpenStep API, but GNUstep does not
// have CoreFoundation.  We use macros pick one or the other.
//

#ifdef GNUSTEP

// Variable declarations
#define DECLARE_INDEX(variable) int variable

// Range operations
#define MAKE_RANGE(location, length) \
     NSMakeRange((location), (length))

// Array operations
#define ARRAY_GET_VALUE_AT_INDEX(array, index) \
     [(array) objectAtIndex: (index)]

#define ARRAY_APPEND_VALUE(array, object) \
     [(array) addObject: (object)]

#define ARRAY_INSERT_VALUE_AT_INDEX(array, index, object) \
     [(array) insertObject: (object) atIndex: (index)]

#define ARRAY_RANGE_CONTAINS_VALUE(array, range, object) \
     bsinternal_array_range_contains_value((array), (range), (object))

#define ARRAY_GET_COUNT(array) \
     [(array) count]

static inline BOOL 
bsinternal_array_range_contains_value(NSArray *array, NSRange range, id object)
{
  int i;
  for (i = range.location; i < range.location + range.length; ++i) {
    id o = [array objectAtIndex: i];
    if ([o isEqual: object]) return YES;
  }
  return NO;
}

// Set operations
#define SET_CONTAINS_VALUE(set, object) \
     [(set) containsObject: (object)]

// byte swapping --TODO--
#define CFSwapInt32BigToHost(x) (x)

// HFS file types --TODO--
#define NSHFSTypeOfFile(file) (file)

#else

// Variable declarations
#define DECLARE_INDEX(variable) CFIndex variable

// Range operations
#define MAKE_RANGE(location, length) \
     CFRangeMake( (location), (length) )

// Array operations
#define ARRAY_GET_VALUE_AT_INDEX(array, index) \
     CFArrayGetValueAtIndex( (CFMutableArrayRef) (array), (index) )

#define ARRAY_APPEND_VALUE(array, object) \
     CFArrayAppendValue ( (CFMutableArrayRef) (array), (object) )

#define ARRAY_INSERT_VALUE_AT_INDEX(array, index, object) \
     CFArrayInsertValueAtIndex ( (CFMutableArrayRef) (array), (index), (object) )

#define ARRAY_RANGE_CONTAINS_VALUE(array, range, object) \
     CFArrayContainsValue ( (CFArrayRef) (array), (range), (object) )

#define ARRAY_GET_COUNT(array) \
     CFArrayGetCount ( (CFArrayRef) (array) )

// Set operations
#define SET_CONTAINS_VALUE(set, object) \
     CFSetContainsValue( (CFSetRef) (set), (object));

#endif /* GNUSTEP */

