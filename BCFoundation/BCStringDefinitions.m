//
//  BCStringDefinitions.m
//  BioCocoa
//
//  Created by Koen van der Drift on Mon Mar 21 2005.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import <Foundation/Foundation.h>

// BCSymbol Strings
NSString * const BCSymbolNameProperty =				@"Name";
NSString * const BCSymbolMonoisotopicMassProperty = @"MonoisotopicMass";
NSString * const BCSymbolAverageMassProperty =		@"AverageMass";
NSString * const BCSymbolThreeLetterCodeProperty =	@"ThreeLetterCode";
NSString * const BCSymbolpKaProperty =				@"pKa";
NSString * const BCSymbolKyteDoolittleProperty =	@"KyteDoolittle";
NSString * const BCSymbolHoppWoodsProperty =		@"HoppWoods";
NSString * const BCSymbolRepresentsProperty =		@"Represents";
NSString * const BCSymbolRepresentedByProperty =	@"Represented by";
NSString * const BCSymbolAllComplementsProperty =	@"All Complements";
NSString * const BCSymbolComplementProperty =		@"Complement";
NSString * const BCSymbolSymbolProperty =			@"Symbol";



// BCAlignment Strings

// Properties
NSString * const BCGapPenaltyProperty =				@"BCGapPenaltyProperty";
// possible values
NSString * const BCDefaultGapPenalty =				@"BCDefaultGapPenalty";
NSString * const BCAffineGapPenalty =				@"BCAffineGapPenalty";

// substitution matrix
NSString * const BCSubstitutionMatrixProperty =		@"BCSubstitutionMatrixProperty";

// NSNumbers
NSString * const BCDefaultGapPenaltyProperty =		@"BCDefaultGapPenaltyProperty";
NSString * const BCGapOpenPenaltyProperty =			@"BCGapOpenPenaltyProperty";
NSString * const BCGapExtensionPenaltyProperty =	@"BCGapExtensionPenaltyProperty";

//
// Enhancements to NSString for URL requests
//
#ifndef GNUSTEP
@implementation NSString (BCURLEscaping)

- (NSString *)stringWithURLPercentEscape
{
  return [(NSString *) CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)[[self mutableCopy] autorelease], NULL, CFSTR("￼=,!$&'()*+;@?\n\"<>#\t :/"),kCFStringEncodingUTF8) autorelease];
}

- (NSString *)stringWithReplaceURLPercentEscape
{
  return [[self stringByReplacingOccurrencesOfString:@"+" withString:@" "] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
#endif


