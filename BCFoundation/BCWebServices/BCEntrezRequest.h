//
//  BCEntrezRequest.h
//  BioCocoa
//
//  Created by Scott Christley on 10/23/13.
//  Copyright (c) 2013 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/*!
 @header
 @abstract Programmatic access to the NCBI databases through Entrez (E-utilities)
 @discussion NCBI's programming manual for Entrez is available at
 http://www.ncbi.nlm.nih.gov/books/NBK25501/
 NCBI requests that precautions are taken to avoid overloading the server including
 no more than 3 requests per second, limit large jobs to the weekend or outside
 normal business hours, and batch download if requesting a large number of records.
 BioCocoa does not automatically enforce these rules, you should write your program
 to avoid violating them, and use the batch methods provided by BCEntrezRequest.
 */

#import <Foundation/Foundation.h>

// BCEntrezRequest Strings
/*!
 @constant BCEntrezBaseURL
 @abstract Base URL for Entrez web service
 */
extern NSString * const BCEntrezBaseURL;
/*!
 @constant BCEntrezInfo
 @abstract EInfo service (database statistics)
 */
extern NSString * const BCEntrezInfo;
/*!
 @constant BCEntrezSearch
 @abstract ESearch service (text searches)
 */
extern NSString * const BCEntrezSearch;
/*!
 @constant BCEntrezPost
 @abstract EPost service (UID uploads)
 */
extern NSString * const BCEntrezPost;
/*!
 @constant BCEntrezSummary
 @abstract ESummary service (document summary downloads)
 */
extern NSString * const BCEntrezSummary;
/*!
 @constant BCEntrezFetch
 @abstract EFetch service (data record downloads)
 */
extern NSString * const BCEntrezFetch;
/*!
 @constant BCEntrezLink
 @abstract ELink service (Entrez links)
 */
extern NSString * const BCEntrezLink;
/*!
 @constant BCEntrezGQuery
 @abstract GQuery service (global query)
 */
extern NSString * const BCEntrezGQuery;
/*!
 @constant BCEntrezSpell
 @abstract ESpell service (spelling suggestions)
 */
extern NSString * const BCEntrezSpell;
/*!
 @constant BCEntrezCitMatch
 @abstract ECitMatch service (batch citation searching in PubMed)
 */
extern NSString * const BCEntrezCitMatch;

/*!
 @class      BCEntrezRequest
 @abstract   Class that manages a data request to NCBI's Entrez web service
 @discussion This class provides access to the 9 E-utilities provided by NCBI,
 including EInfo, ESearch, EPost, ESummary, EFetch, ELink, EGQuery, ESpell
 and ECitMatch. The URL request and data transfer is managed automatically,
 however this class performs only minimal interpretation of the XML data
 to determine if the request was successful or not.
 */

@interface BCEntrezRequest : NSObject
{
  BOOL isDone;
  NSMutableData *receivedData;
  NSXMLDocument *xmlDoc;
  NSString *errorString;

  NSURLConnection *batchConnection;
  FILE *batchFile;
}

- (BOOL)isDone;
- (NSData *)receivedData;
- (NSXMLDocument *)xmlData;
- (NSString *)errorString;

- (NSXMLDocument *)performEntrezInfoRequest:(NSString *)dbName;

- (NSXMLDocument *)performEntrezSearchRequest:(NSString *)dbName
                                   parameters:(NSDictionary *)parameters
                                   useHistory:(BOOL)aFlag;
- (NSXMLDocument *)performEntrezSearchRequest:(NSString *)dbName
                                   parameters:(NSDictionary *)parameters;

- (NSXMLDocument *)performEntrezPostRequest:(NSString *)dbName idList:(NSArray *)idList;

- (BOOL)performEntrezFetchRequest:(NSString *)dbName
                           idList:(NSArray *)idList
                       parameters:(NSDictionary *)parameters
                          xmlData:(BOOL)xmlFlag;

- (BOOL)performEntrezFetchRequest:(NSString *)dbName
                           idList:(NSArray *)idList
                       parameters:(NSDictionary *)parameters
                  batchModeToFile:(NSString *)filePath;

- (NSXMLDocument *)performEntrezLinkRequest:(NSString *)dbName
                                     fromDB:(NSString *)dbFromName
                                 parameters:(NSDictionary *)parameters;
- (NSXMLDocument *)performEntrezLinkRequest:(NSString *)dbName
                                     fromDB:(NSString *)dbFromName
                                     idList:(NSArray *)idList;

@end
