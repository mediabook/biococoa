//
//  BCEntrezRequest.m
//  BioCocoa
//
//  Created by Scott Christley on 10/23/13.
//  Copyright (c) 2013 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCEntrezRequest.h"
#import "BCStringDefinitions.h"

NSString * const BCEntrezBaseURL = @"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/";
NSString * const BCEntrezInfo = @"einfo.fcgi";
NSString * const BCEntrezSearch = @"esearch.fcgi";
NSString * const BCEntrezPost = @"epost.fcgi";
NSString * const BCEntrezSummary = @"esummary.fcgi";
NSString * const BCEntrezFetch = @"efetch.fcgi";
NSString * const BCEntrezLink = @"elink.fcgi";
NSString * const BCEntrezGQuery = @"egquery.fcgi";
NSString * const BCEntrezSpell = @"espell.fcgi";
NSString * const BCEntrezCitMatch = @"ecitmatch.fcgi";

@implementation BCEntrezRequest

- (void)dealloc
{
  if (receivedData) [receivedData release];
  
  [super dealloc];
}

- (BOOL)isDone { return isDone; }
- (NSData *)receivedData { return receivedData; }
- (NSXMLDocument *)xmlData { return xmlDoc; }
- (NSString *)errorString { return errorString; }


- (void)performRequest: (NSURLRequest *)theRequest
{
  NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
  if (theConnection) {
    // Create the NSMutableData to hold the received data.
    // receivedData is an instance variable declared elsewhere.
    receivedData = [[NSMutableData data] retain];
    xmlDoc = nil;
    isDone = NO;
  } else {
    // Inform the user that the connection failed.
    printf("ERROR: Could not create URL connection to NCBI Entrez.\n");
    isDone = YES;
  }
}

- (void)performSynchronousRequest: (NSURLRequest *)theRequest
{
  [self performRequest: theRequest];
  
  while (![self isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }
}

- (void)performBatchRequest: (NSURLRequest *)theRequest
{
  batchConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
  if (batchConnection) {
    // Create the NSMutableData to hold the received data.
    // receivedData is an instance variable declared elsewhere.
    receivedData = nil;
    xmlDoc = nil;
    isDone = NO;
  } else {
    // Inform the user that the connection failed.
    printf("ERROR: Could not create URL connection to NCBI Entrez.\n");
    isDone = YES;
  }

  while (![self isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }
}

- (NSXMLDocument *)performEntrezInfoRequest:(NSString *)dbName
{
  // form URL
  NSMutableString *requestString = [NSMutableString stringWithFormat: @"%@%@", BCEntrezBaseURL, BCEntrezInfo];
  if (dbName) [requestString appendFormat:@"?db=%@", dbName];
  
  NSURL *theURL = [NSURL URLWithString: requestString];

  // generate request
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  [theRequest setHTTPMethod: @"GET"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setTimeoutInterval: 300];

  // perform request
  [self performSynchronousRequest: theRequest];
  
  NSError *error;
  xmlDoc = [[NSXMLDocument alloc] initWithData: receivedData options:0 error: &error];
  if (!xmlDoc) {
    errorString = [NSString stringWithFormat: @"Invalid XML received from NCBI Entrez: %@\n%@",
                       [error localizedDescription], [[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding] autorelease]];
    return nil;
  }
  
  return xmlDoc;
}

- (NSXMLDocument *)performEntrezSearchRequest:(NSString *)dbName
                                   parameters:(NSDictionary *)parameters
                                   useHistory:(BOOL)aFlag
{
  // database name is required
  if (!dbName) {
    return nil;
  }

  // form URL
  NSMutableString *requestString = [NSMutableString stringWithFormat: @"%@%@", BCEntrezBaseURL, BCEntrezSearch];
  [requestString appendFormat: @"?db=%@", dbName];

  if (aFlag) [requestString appendFormat: @"&usehistory=y"];
  
  int i;
  NSArray *keys = [parameters allKeys];
  for (i = 0; i < [keys count]; ++i) {
    NSString *key = [keys objectAtIndex: i];
    [requestString appendFormat: @"&%@=%@", key, [[parameters objectForKey: key] stringWithURLPercentEscape]];
  }
  printf("%s\n", [requestString UTF8String]);

  NSURL *theURL = [NSURL URLWithString: requestString];
  
  // generate request
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  [theRequest setHTTPMethod: @"GET"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setTimeoutInterval: 300];
  
  // perform request
  [self performSynchronousRequest: theRequest];

  NSError *error;
  xmlDoc = [[NSXMLDocument alloc] initWithData: receivedData options:0 error: &error];
  if (!xmlDoc) {
    errorString = [NSString stringWithFormat: @"Invalid XML received from NCBI Entrez: %@\n%@",
                   [error localizedDescription], [[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding] autorelease]];
    return nil;
  }
  
  return xmlDoc;
}

- (NSXMLDocument *)performEntrezSearchRequest:(NSString *)dbName
                                   parameters:(NSDictionary *)parameters
{
  return [self performEntrezSearchRequest: dbName parameters: parameters useHistory: NO];
}

- (NSXMLDocument *)performEntrezPostRequest:(NSString *)dbName idList:(NSArray *)idList
{
  // database name is required
  if (!dbName) {
    return nil;
  }
  
  // list of ids required
  if ((!idList) || ([idList count] == 0)) {
    return nil;
  }
  
  // form URL
  NSMutableString *requestString = [NSMutableString stringWithFormat: @"%@%@", BCEntrezBaseURL, BCEntrezPost];

  NSURL *theURL = [NSURL URLWithString: requestString];
  
  // generate request
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  [theRequest setHTTPMethod: @"POST"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setTimeoutInterval: 300];

  // body data with id numbers
  NSMutableString *body = [NSMutableString stringWithFormat: @"db=%@", dbName];
  int i;
  for (i = 0; i < [idList count]; ++i) {
    if (i == 0) [body appendFormat: @"&id="];
    else [body appendString: @","];
    [body appendString: [idList objectAtIndex:i]];
  }
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  printf("%s\n", [body UTF8String]);

  // perform request
  [self performSynchronousRequest: theRequest];

  NSError *error;
  xmlDoc = [[NSXMLDocument alloc] initWithData: receivedData options:0 error: &error];
  if (!xmlDoc) {
    errorString = [NSString stringWithFormat: @"Invalid XML received from NCBI Entrez: %@\n%@",
                   [error localizedDescription], [[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding] autorelease]];
    return nil;
  }
  
  return xmlDoc;
}

- (NSXMLDocument *)performEntrezLinkRequest:(NSString *)dbName
                                     fromDB:(NSString *)dbFromName
                                 parameters:(NSDictionary *)parameters
{
  // database name is required
  if (!dbName) {
    return nil;
  }
  if (!dbFromName) {
    return nil;
  }
  
  // form URL
  NSMutableString *requestString = [NSMutableString stringWithFormat: @"%@%@", BCEntrezBaseURL, BCEntrezLink];
  [requestString appendFormat: @"?db=%@&dbfrom=%@", dbName, dbFromName];
  
  int i;
  NSArray *keys = [parameters allKeys];
  for (i = 0; i < [keys count]; ++i) {
    NSString *key = [keys objectAtIndex: i];
    [requestString appendFormat: @"&%@=%@", key, [[parameters objectForKey: key] stringWithURLPercentEscape]];
  }
  printf("%s\n", [requestString UTF8String]);
  
  NSURL *theURL = [NSURL URLWithString: requestString];
  
  // generate request
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  [theRequest setHTTPMethod: @"GET"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setTimeoutInterval: 300];
  
  // perform request
  [self performSynchronousRequest: theRequest];
  
  NSError *error;
  xmlDoc = [[NSXMLDocument alloc] initWithData: receivedData options:0 error: &error];
  if (!xmlDoc) {
    errorString = [NSString stringWithFormat: @"Invalid XML received from NCBI Entrez: %@\n%@",
                   [error localizedDescription], [[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding] autorelease]];
    return nil;
  }
  
  return xmlDoc;
}

- (NSXMLDocument *)performEntrezLinkRequest:(NSString *)dbName
                                     fromDB:(NSString *)dbFromName
                                     idList:(NSArray *)idList
{
  // database name is required
  if (!dbName) {
    return nil;
  }
  if (!dbFromName) {
    return nil;
  }
  
  // list of ids required
  if ((!idList) || ([idList count] == 0)) {
    return nil;
  }

  // form URL
  NSMutableString *requestString = [NSMutableString stringWithFormat: @"%@%@", BCEntrezBaseURL, BCEntrezLink];
  NSURL *theURL = [NSURL URLWithString: requestString];

  // generate request
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  [theRequest setHTTPMethod: @"POST"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setTimeoutInterval: 300];
  
  // body data with id numbers
  NSMutableString *body = [NSMutableString stringWithFormat: @"db=%@&dbfrom=%@", dbName, dbFromName];
  int i;
  for (i = 0; i < [idList count]; ++i) {
    if (i == 0) [body appendFormat: @"&id="];
    else [body appendString: @","];
    [body appendString: [idList objectAtIndex:i]];
  }
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  printf("%s\n", [body UTF8String]);

  // perform request
  [self performSynchronousRequest: theRequest];
  
  NSError *error;
  xmlDoc = [[NSXMLDocument alloc] initWithData: receivedData options:0 error: &error];
  if (!xmlDoc) {
    errorString = [NSString stringWithFormat: @"Invalid XML received from NCBI Entrez: %@\n%@",
                   [error localizedDescription], [[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding] autorelease]];
    return nil;
  }
  
  return xmlDoc;
}

- (BOOL)performEntrezFetchRequest:(NSString *)dbName
                           idList:(NSArray *)idList
                       parameters:(NSDictionary *)parameters
                          xmlData:(BOOL)xmlFlag
{
  // database name is required
  if (!dbName) {
    return NO;
  }
  
  // list of ids required
  if ((!idList) || ([idList count] == 0)) {
    return NO;
  }
  
  // form URL
  NSMutableString *requestString = [NSMutableString stringWithFormat: @"%@%@", BCEntrezBaseURL, BCEntrezFetch];

  NSURL *theURL = [NSURL URLWithString: requestString];
  
  // generate request
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  [theRequest setHTTPMethod: @"POST"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setTimeoutInterval: 300];
  
  // body data with id numbers
  NSMutableString *body = [NSMutableString stringWithFormat: @"db=%@", dbName];
  int i;
  NSArray *keys = [parameters allKeys];
  for (i = 0; i < [keys count]; ++i) {
    NSString *key = [keys objectAtIndex: i];
    [body appendFormat: @"&%@=%@", key, [[parameters objectForKey: key] stringWithURLPercentEscape]];
  }

  for (i = 0; i < [idList count]; ++i) {
    if (i == 0) [body appendFormat: @"&id="];
    else [body appendString: @","];
    [body appendString: [idList objectAtIndex:i]];
  }
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  printf("%s\n", [body UTF8String]);
  
  // perform request
  [self performSynchronousRequest: theRequest];
  
  if (xmlFlag) {
    NSError *error;
    xmlDoc = [[NSXMLDocument alloc] initWithData: receivedData options:0 error: &error];
    if (!xmlDoc) {
      errorString = [NSString stringWithFormat: @"Invalid XML received from NCBI Entrez: %@\n%@",
                   [error localizedDescription], [[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding] autorelease]];
      return NO;
    }
  }

  return YES;
}

- (BOOL)performEntrezFetchRequest:(NSString *)dbName
                           idList:(NSArray *)idList
                       parameters:(NSDictionary *)parameters
                  batchModeToFile:(NSString *)filePath
{
  // database name is required
  if (!dbName) {
    return NO;
  }
  
  // list of ids required
  if ((!idList) || ([idList count] == 0)) {
    return NO;
  }

  // filePath required
  if (!filePath) {
    return NO;
  }
  
  batchFile = fopen([filePath UTF8String], "w");

  BOOL done = NO;
  int currentBatch = 0;
  while (!done) {
    // form URL
    NSMutableString *requestString = [NSMutableString stringWithFormat: @"%@%@", BCEntrezBaseURL, BCEntrezFetch];
    
    NSURL *theURL = [NSURL URLWithString: requestString];
    
    // generate request
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
    [theRequest setHTTPMethod: @"POST"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setTimeoutInterval: 300];
    
    // body data with id numbers
    NSMutableString *body = [NSMutableString stringWithFormat: @"db=%@", dbName];
    int i;
    NSArray *keys = [parameters allKeys];
    for (i = 0; i < [keys count]; ++i) {
      NSString *key = [keys objectAtIndex: i];
      [body appendFormat: @"&%@=%@", key, [[parameters objectForKey: key] stringWithURLPercentEscape]];
    }
    
    // 1000 ids at a time
    for (i = currentBatch; (i < [idList count]) && ((i - currentBatch) < 1000); ++i) {
      if (i == currentBatch) [body appendFormat: @"&id="];
      else [body appendString: @","];
      [body appendString: [idList objectAtIndex:i]];
    }
    NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
    [theRequest setHTTPBody: bodyData];
    printf("(%d, %d) %s\n", i, currentBatch, [body UTF8String]);
    
    // perform request
    [self performBatchRequest: theRequest];

    currentBatch = i;
    if (currentBatch >= [idList count]) done = YES;
  }
  
  fclose(batchFile);

  return YES;
}

//
// Receive asynchronous data
//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
  // This method is called when the server has determined that it
  // has enough information to create the NSURLResponse.
  
  // It can be called multiple times, for example in the case of a
  // redirect, so each time we reset the data.
  
  // zero out the data.
  if (connection == batchConnection) {
  } else {
    [receivedData setLength:0];
  }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  if (connection == batchConnection) {
    // write data to the batch file
    fwrite([data bytes], [data length], sizeof(char), batchFile);
  } else {
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:data];
  }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
  // release the connection, and the data object
  [connection release];
  [receivedData release];
  receivedData = nil;
  isDone = YES;
  
  // inform the user
  NSLog(@"Connection failed! Error - %@ %@",
        [error localizedDescription],
        [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  // release the connection, and indicate done
  [connection release];
  isDone = YES;
}

@end
