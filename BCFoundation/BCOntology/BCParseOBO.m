//
//  BCParseOBO.h
//  BioCocoa
//
//  Load ontologies in OBO format.
//
//  Created by Scott Christley on 02/08/2011.
//  Copyright (c) 2011 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCParseOBO.h"


@implementation BCParseOBO

- (void)dealloc
{
	if (header) [header release];
	if (stanzas) [stanzas release];
	[super dealloc];
}

// Clean up string
- (NSString *)cleanString: (NSString *)aString
{
	NSString *s = aString;

	// strip comment
	NSRange r = [s rangeOfString: @"!"];
	if (r.location != NSNotFound) {
		while (r.location != NSNotFound) {
			if ([s characterAtIndex: r.location - 1] == '\\') {
				NSRange r1 = NSMakeRange(r.location + 1, [s length] - r.location + 1);
				r = [s rangeOfString: @"!" options: NSCaseInsensitiveSearch range: r1];
			} else
				break;
		}
		if (r.location != NSNotFound) s = [s substringToIndex: r.location];
	}

	NSCharacterSet *trimSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
	s = [s stringByTrimmingCharactersInSet: trimSet];

	return s;
}

// Parse the header fields at beginning of file
- (int)parseHeader: (NSArray *)lines
{
	header = [NSMutableDictionary dictionary];
	
	int i;
	for (i = 0; i < [lines count]; ++i) {
		NSString *s = [lines objectAtIndex: i];

		if ([s length] == 0) continue;
		if ([s characterAtIndex: 0] == '!') continue;

		NSRange r = [s rangeOfString: @":"];
		if (r.location == NSNotFound) {
			r = [s rangeOfString: @"]"];
			// stop when find first stanza
			if ((r.location != NSNotFound) && ([s characterAtIndex: 0] == '['))
				return i;
			else
				continue;
		}
		
		NSString *tag = [self cleanString: [s substringToIndex: r.location]];
		NSString *value = [self cleanString: [s substringFromIndex: r.location + 1]];
    
    if ([tag isEqualToString: @"subsetdef"]) {
      NSMutableArray *a = [header objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [header setObject: a forKey: tag];
      }
      [a addObject: value];
    } else if ([tag isEqualToString: @"synonymtypedef"]) {
      NSMutableArray *a = [header objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [header setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"idspace"]) {
      NSMutableArray *a = [header objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [header setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"id-mapping"]) {
      NSMutableArray *a = [header objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [header setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"import"]) {
      NSMutableArray *a = [header objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [header setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else
      [header setObject: value forKey: tag];
	}

	return i;
}

// Parse a stanza section
- (int)parseStanza: (NSArray *)lines atLine: (int)start
{
	id stanza = [NSMutableDictionary dictionary];
	
	int i = start;
	NSString *s = [self cleanString: [lines objectAtIndex: i]];
	[stanza setObject: s forKey: @"stanza"];

	for (i = start + 1; i < [lines count]; ++i) {
		s = [lines objectAtIndex: i];

		if ([s length] == 0) continue;
		if ([s characterAtIndex: 0] == '!') continue;

		NSRange r = [s rangeOfString: @":"];
		if (r.location == NSNotFound) {
			r = [s rangeOfString: @"]"];
			// stop when find next stanza
			if ((r.location != NSNotFound) && ([s characterAtIndex: 0] == '['))
				break;
			else
				continue;
		}

		NSString *tag = [self cleanString: [s substringToIndex: r.location]];
		NSString *value = [self cleanString: [s substringFromIndex: r.location + 1]];

    if ([tag isEqualToString: @"alt_id"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];
    } else if ([tag isEqualToString: @"subset"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"synonym"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];
    } else if ([tag isEqualToString: @"xref"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"is_a"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"consider"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"replaced_by"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else if ([tag isEqualToString: @"disjoint_from"]) {
      NSMutableArray *a = [stanza objectForKey: tag];
      if (!a) {
        a = [NSMutableArray array];
        [stanza setObject: a forKey: tag];
      }
      [a addObject: value];      
    } else      
      [stanza setObject: value forKey: tag];
	}
	
  [stanzas setObject: stanza forKey: [stanza objectForKey: @"id"]];

	return i;
}

// Load the ontology
- (NSDictionary *)loadOntologyFile:(NSString *)fileName
{
	NSError *e;
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	NSString *s = [NSString stringWithContentsOfFile: fileName encoding: NSUTF8StringEncoding error: &e];

	if (!s) {
		printf("Could not open file: %s\n", [fileName UTF8String]);
		printf("%s\n", [[e localizedDescription] UTF8String]);
		//NSRunAlertPanel(@"File Open Error", [e localizedDescription], nil, nil, nil);
		[pool release];
		return nil;
	}

	// split into lines
	NSArray *a = [s componentsSeparatedByString: @"\n"];
	//printf("%d\n", [a count]);

	int i = [self parseHeader: a];
	//printf("%s\n", [[header description] UTF8String]);

	stanzas = [NSMutableDictionary dictionary];
	while (i < [a count]) {
		i = [self parseStanza: a atLine: i];
	}
	//printf("%s\n", [[[stanzas objectAtIndex: 0] description] UTF8String]);

  NSMutableDictionary *ontology = [NSMutableDictionary new];
  [ontology setObject: header forKey: @"header"];
  [ontology setObject: stanzas forKey: @"classes"];
  
	[pool release];
	return ontology;
}

@end
