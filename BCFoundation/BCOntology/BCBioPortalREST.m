//
//  BCBioPortalREST.m
//  BioCocoa
//
//  Web service to BioPortal ontologies.
//
//  Created by Scott Christley on 2/9/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCBioPortalREST.h"
#import "BCPreferences.h"
#import "BCParseOBO.h"


// BCBioPortalREST Strings
NSString * const BCBioPortalRESTURL = @"http://rest.bioontology.org/bioportal/";
NSString * const BCBioPortalOntologyList = @"ontologies?apikey=%@";
NSString * const BCBioPortalGetOntology = @"virtual/ontology/%@?apikey=%@";
NSString * const BCBioPortalGetOntologyVersion = @"ontologies/%@?apikey=%@";
NSString * const BCBioPortalGetAllOntologyVersions = @"ontologies/versions/%@?apikey=%@";
NSString * const BCBioPortalDownloadOntology = @"virtual/download/%@?apikey=%@";
NSString * const BCBioPortalDownloadOntologyVersion = @"ontologies/download/%@?apikey=%@";
NSString * const BCBioPortalOntologyCategories = @"categories?apikey=%@";
NSString * const BCBioPortalOntologyGroups = @"groups?apikey=%@";

NSString * const BCBioPortalOntologyDirectory = @"Ontology";


//
// A helper class for the URL connection to BioPortal
//
@interface BCBioPortalConnection : NSObject
{
  BOOL isDone;
  NSMutableData *receivedData;
}

- (void)performRequest: (NSURLRequest *)theRequest;
- (BOOL)isDone;
- (NSData *)receivedData;

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;

@end


@implementation BCBioPortalREST

- initWithAPIKey: (NSString *)aKey;
{
  [super init];
  
  if (!aKey) {
    printf("ERROR: API key is required to access BioPortal REST.\n");
    return nil;
  } else apiKey = aKey;
  [apiKey retain];

  return self;
}

- (void)dealloc
{
  if (apiKey) [apiKey release];
  if (ontologyList) [ontologyList release];
  
  [super dealloc];
}

- (void)clearCache
{
  if (ontologyList) [ontologyList release];
  ontologyList = nil;
}

- (NSArray *)ontologyList
{
  if (ontologyList) return ontologyList;
  
  NSMutableString *s = [NSMutableString stringWithString: BCBioPortalRESTURL];
  [s appendFormat: BCBioPortalOntologyList, apiKey];
  
  NSURL *u = [NSURL URLWithString: s];
  NSURLRequest *theRequest = [NSURLRequest requestWithURL: u];
  BCBioPortalConnection *theConnection = [BCBioPortalConnection new];
  
  [theConnection performRequest: theRequest];
  
  while (![theConnection isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }
  
  NSError *error = nil;
  id xml = [[[NSXMLDocument alloc] initWithData:[theConnection receivedData] options:NSXMLNodeOptionsNone error:&error] autorelease];
  [theConnection release];
  if (!xml) return nil;

  //printf("%s\n", [[xml description] UTF8String]);                                                                                                             
  //[[theConnection receivedData] writeToFile: @"test.xml" atomically: YES];
  
  NSXMLElement *root = [xml rootElement];
  //printf("root: %s\n", [[root name] UTF8String]);
  NSArray *a = [root elementsForName: @"data"];
  if (!a) return nil;

  //printf("data: %d\n", [a count]);
  a = [[a objectAtIndex: 0] elementsForName: @"list"];
  if (!a) return nil;
  
  //printf("list: %d\n", [a count]);
  a = [[a objectAtIndex: 0] elementsForName: @"ontologyBean"];
  if (!a) return nil;

  //printf("ontologyBean: %d\n", [a count]);

  ontologyList = [[NSMutableArray array] retain];
  int i, j, k;
  for (i = 0; i < [a count]; ++i) {
    NSArray *b = [[a objectAtIndex: i] children];
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    for (j = 0; j < [b count]; ++j) {
      NSString *name = [[b objectAtIndex: j] name];
      if ([name isEqualToString: @"categoryIds"]) {
        NSArray *c = [[b objectAtIndex: j] children];
        NSMutableArray *idList = [NSMutableArray array];
        for (k = 0; k < [c count]; ++k) {
          [idList addObject: [NSNumber numberWithInt: [[[c objectAtIndex: k] stringValue] intValue]]];
        }
        [d setObject: idList forKey: [[b objectAtIndex: j] name]];        
      } else if ([name isEqualToString: @"groupIds"]) {
        NSArray *c = [[b objectAtIndex: j] children];
        NSMutableArray *idList = [NSMutableArray array];
        for (k = 0; k < [c count]; ++k) {
          [idList addObject: [NSNumber numberWithInt: [[[c objectAtIndex: k] stringValue] intValue]]];
        }
        [d setObject: idList forKey: [[b objectAtIndex: j] name]];                
      } else {
        [d setObject: [[b objectAtIndex: j] stringValue] forKey: [[b objectAtIndex: j] name]];
      }
    }
    [ontologyList addObject: d];
  }
  
  return ontologyList;
}

- (NSDictionary *)getOntology: (NSString *)anID
{
  int i;
  
  if (!ontologyList) [self ontologyList];

  if (ontologyList) {
    for (i = 0; i < [ontologyList count]; ++i) {
      NSDictionary *d = [ontologyList objectAtIndex: i];
      NSString *s = [d objectForKey: @"ontologyId"];
      if ([anID isEqualToString: s]) return d;
    }
  } else {
    printf("ERROR: Unable to load ontology list.\n");
  }
  
  return nil;
}

- (NSXMLElement *)getOntologyVersionXML: (NSString *)aVersionID
{
  NSMutableString *s = [NSMutableString stringWithString: BCBioPortalRESTURL];
  [s appendFormat: BCBioPortalGetOntologyVersion, aVersionID, apiKey];
  
  NSURL *u = [NSURL URLWithString: s];
  NSURLRequest *theRequest = [NSURLRequest requestWithURL: u];
  BCBioPortalConnection *theConnection = [BCBioPortalConnection new];
  
  [theConnection performRequest: theRequest];
  
  while (![theConnection isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }
  
  NSError *error = nil;
  id xml = [[[NSXMLDocument alloc] initWithData:[theConnection receivedData] options:NSXMLNodeOptionsNone error:&error] autorelease];
  [theConnection release];
  if (!xml) return nil;

  NSXMLElement *root = [xml rootElement];
  NSArray *a = [root elementsForName: @"data"];
  if (!a) return nil;

  a = [[a objectAtIndex: 0] elementsForName: @"ontologyBean"];
  if (!a) return nil;

  return [a objectAtIndex: 0];
}

- (NSDictionary *)getOntologyVersion: (NSString *)aVersionID
{
  NSXMLElement *xml = [self getOntologyVersionXML: aVersionID];

  return nil;
}

- (NSDictionary *)getAllOntologyVersions: (NSString *)anID
{
  return nil;
}

- (BOOL)downloadOntology: (NSString *)anID
{
  NSMutableString *s = [NSMutableString stringWithString: BCBioPortalRESTURL];
  [s appendFormat: BCBioPortalDownloadOntology, anID, apiKey];
  
  NSURL *u = [NSURL URLWithString: s];
  NSURLRequest *theRequest = [NSURLRequest requestWithURL: u];
  BCBioPortalConnection *theConnection = [[BCBioPortalConnection new] autorelease];
  
  [theConnection performRequest: theRequest];
  
  while (![theConnection isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }

  // Save in shared ontology directory
  NSDictionary *o = [self getOntology: anID];
  if (!o) return NO;
  NSString *format = [o objectForKey: @"format"];
  NSString *filename = [NSString stringWithFormat: @"%@.%@", anID, format];
  NSString *destinationFilename = [[BCPreferences sharedDataSubdirectory: BCBioPortalOntologyDirectory]
                         stringByAppendingPathComponent:filename];
  //printf("%s\n", [destinationFilename UTF8String]);
  [[theConnection receivedData] writeToFile: destinationFilename atomically: YES];
  
  return YES;
}

- (NSDictionary *)downloadOntologyVersion: (NSString *)aVersionID
{
  return nil;
}

- (NSArray *)ontologyCategories
{
  return nil;
}

- (NSArray *)ontologyGroups
{
  return nil;
}

#if 0
#pragma mark == LOAD ONTOLOGY ==
#endif

- (NSDictionary *)loadOntology: (NSString *)anID;
{
  // check for existing OBO or OWL file
  NSString *filename = [NSString stringWithFormat: @"%@.OBO", anID];
  NSString *filePath = [[BCPreferences sharedDataSubdirectory: BCBioPortalOntologyDirectory]
                                   stringByAppendingPathComponent:filename];
  if ([[NSFileManager defaultManager] fileExistsAtPath: filePath]) {
    BCParseOBO *parser = [[BCParseOBO new] autorelease];
    return [parser loadOntologyFile: filePath];
  }
  
  filename = [NSString stringWithFormat: @"%@.OWL", anID];
  filePath = [[BCPreferences sharedDataSubdirectory: BCBioPortalOntologyDirectory]
                                   stringByAppendingPathComponent:filename];
  if ([[NSFileManager defaultManager] fileExistsAtPath: filePath]) {
  }
   
  filename = [NSString stringWithFormat: @"%@.OWL-DL", anID];
  filePath = [[BCPreferences sharedDataSubdirectory: BCBioPortalOntologyDirectory]
                         stringByAppendingPathComponent:filename];
  if ([[NSFileManager defaultManager] fileExistsAtPath: filePath]) {
  
  }

  filename = [NSString stringWithFormat: @"%@.OWL-FULL", anID];
  filePath = [[BCPreferences sharedDataSubdirectory: BCBioPortalOntologyDirectory]
              stringByAppendingPathComponent:filename];
  if ([[NSFileManager defaultManager] fileExistsAtPath: filePath]) {
    
  }
  
  return nil;
}

@end


//
// A helper class for the URL connection to BioPortal
//

@implementation BCBioPortalConnection

- (void)dealloc
{
  if (receivedData) [receivedData release];

  [super dealloc];
}

- (void)performRequest: (NSURLRequest *)theRequest
{
  NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
  if (theConnection) {
    // Create the NSMutableData to hold the received data.                                                                                                      
    // receivedData is an instance variable declared elsewhere.                                                                                                 
    receivedData = [[NSMutableData data] retain];
    isDone = NO;
  } else {
    // Inform the user that the connection failed.                                                                                                              
    printf("ERROR: Could not create URL connection to BioPortal.\n");
    isDone = YES;
  }
}

- (BOOL)isDone { return isDone; }
- (NSData *)receivedData { return receivedData; }

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
  // This method is called when the server has determined that it                                                                                               
  // has enough information to create the NSURLResponse.                                                                                                        
  
  // It can be called multiple times, for example in the case of a                                                                                              
  // redirect, so each time we reset the data.                                                                                                                  
  
  // zero out the data.                                                                                                   
  [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  // Append the new data to receivedData.                                                                                                                       
  // receivedData is an instance variable declared elsewhere.                                                                                                   
  [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
  // release the connection, and the data object                                                                                                                
  [connection release];
  [receivedData release];
  receivedData = nil;
  isDone = YES;
  
  // inform the user                                                                                                                                            
  NSLog(@"Connection failed! Error - %@ %@",
        [error localizedDescription],
        [[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  // release the connection, and indicate done                                                                                                                
  [connection release];
  isDone = YES;
}

@end
