//
//  BCBioPortalREST.h
//  BioCocoa
//
//  Web service to BioPortal ontologies.
//
//  Created by Scott Christley on 2/9/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#import <Foundation/Foundation.h>

/*!
 @header
 @abstract Access BioPortal REST web service. 
 */

/*!
 @class      BCBioPortalREST
 @abstract   Wrapper class to access BioPortal REST web service.
 @discussion The BioPortal web service provides ontologies.
 */

@interface BCBioPortalREST : NSObject {
  NSString *apiKey;
  
  NSMutableArray *ontologyList;
}

/*!
 @method     initWithAPIKey:
 @abstract   Initialize BioPortal.
 @discussion An API key is required to access BioPortal REST.
 */
- initWithAPIKey: (NSString *)aKey;

/*!
 @method     clearCache
 @abstract   Clears all of the cached data in memory.
 @discussion When data is downloaded from BioPortal but not
 saved to disk, such as ontologyList, then that data is held
 in memory. Future requests return the cached data instead of
 performing a new URL request. Clearing the cache will delete
 this in-memory data so the next time any data is requested
 that will force a URL request.
 */
- (void)clearCache;

/*!
 @method     ontologyList
 @abstract   Returns a list of all ontologies available at BioPortal.
 @discussion The ontology list recieved from BioPortal REST is an XML
 document which is post-processed into an OpenStep property list.
 */
- (NSArray *)ontologyList;

- (NSDictionary *)getOntology: (NSString *)anID;

- (NSXMLElement *)getOntologyVersionXML: (NSString *)aVersionID;
- (NSDictionary *)getOntologyVersion: (NSString *)aVersionID;
- (NSDictionary *)getAllOntologyVersions: (NSString *)anID;

- (BOOL)downloadOntology: (NSString *)anID;
- (NSDictionary *)downloadOntologyVersion: (NSString *)aVersionID;

- (NSArray *)ontologyCategories;
- (NSArray *)ontologyGroups;

- (NSDictionary *)loadOntology: (NSString *)anID;

@end


/*!
 @constant BCBioPortalRESTURL
 @abstract Main BioPortal REST URL
 */
extern NSString * const BCBioPortalRESTURL;

/*!
 @constant BCBioPortalOntologyList
 @abstract BioPortal ontology list URL
 */
extern NSString * const BCBioPortalOntologyList;

/*!
 @constant BCBioPortalGetOntology
 @abstract BioPortal get ontology URL
 */
extern NSString * const BCBioPortalGetOntology;

/*!
 @constant BCBioPortalGetOntologyVersion
 @abstract BioPortal get ontology version URL
 */
extern NSString * const BCBioPortalGetOntologyVersion;

/*!
 @constant BCBioPortalGetAllOntologyVersions
 @abstract BioPortal get all ontology versions URL
 */
extern NSString * const BCBioPortalGetAllOntologyVersions;

/*!
 @constant BCBioPortalDownloadOntology
 @abstract BioPortal download ontology URL
 */
extern NSString * const BCBioPortalDownloadOntology;

/*!
 @constant BCBioPortalDownloadOntologyVersion
 @abstract BioPortal download ontology version URL
 */
extern NSString * const BCBioPortalDownloadOntologyVersion;

/*!
 @constant BCBioPortalOntologyCategories
 @abstract BioPortal ontology categoreis URL
 */
extern NSString * const BCBioPortalOntologyCategories;

/*!
 @constant BCBioPortalOntologyGroups
 @abstract BioPortal ontology groups URL
 */
extern NSString * const BCBioPortalOntologyGroups;

/*!
 @constant BCBioPortalOntologyDirectory
 @abstract Directory where downloaded ontology files are stored.
 */
extern NSString * const BCBioPortalOntologyDirectory;
