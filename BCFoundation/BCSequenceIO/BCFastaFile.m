//
//  BCFastaFile.m
//  BioCocoa
//
//  Created by Scott Christley on 10/18/13.
//  Copyright (c) 2013 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCFastaFile.h"
#import "BCAnnotation.h"
#import "BCSequence.h"

@implementation BCFastaFile

- initWithFileForReading:(NSString *)filePath
{
  [super initWithFileForReading: filePath];
  
  // open file
  fileHandle = fopen([filePath UTF8String], "r");
  if (!fileHandle) {
    NSLog(@"Could not open file: %@\n", filePath);
    [self dealloc];
    return nil;
  }
  
  return self;
}

- (BCSequence *)nextSequence
{
  // find beginning of fasta entry
  char c;
  while (!feof(fileHandle)) {
    fread(&c, sizeof(char), 1, fileHandle);
    if (feof(fileHandle)) break;

    if (c == '>') break;
  }

  // no more sequences
  if (feof(fileHandle)) return nil;
  
  // fasta header
  NSMutableString *fastaHeader = [NSMutableString string];
  fread(&c, sizeof(char), 1, fileHandle);
  while ((c != '\n') && (!feof(fileHandle))) {
    [fastaHeader appendFormat: @"%c", c];
    fread(&c, sizeof(char), 1, fileHandle);
  }
  BCAnnotation *fasta = [BCAnnotation annotationWithName:BCAnnotationIdentity content:fastaHeader];

  // sequence data
  NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
  NSMutableString *sequenceString = [NSMutableString string];
  fread(&c, sizeof(char), 1, fileHandle);
  while ((c != '>') && (!feof(fileHandle))) {
    if (![charSet characterIsMember:c]) [sequenceString appendFormat: @"%c", c];
    
    fread(&c, sizeof(char), 1, fileHandle);
  }

  if (c == '>') fseeko(fileHandle, -1, SEEK_CUR);
  
  BCSequence *seq = [BCSequence sequenceWithString: sequenceString];
  [seq addAnnotation: fasta];
  
  return seq;
}

@end
