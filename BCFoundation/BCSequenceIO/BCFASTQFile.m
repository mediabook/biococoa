//
//  BCFASTQFile.m
//  BioCocoa
//
//  Created by Scott Christley on 10/21/13.
//  Copyright (c) 2013 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCFASTQFile.h"
#import "BCAnnotation.h"
#import "BCSequence.h"

@implementation BCFASTQFile

- initWithFileForReading:(NSString *)filePath
{
  [super initWithFileForReading: filePath];
  
  // open file
  fileHandle = fopen([filePath UTF8String], "r");
  if (!fileHandle) {
    NSLog(@"Could not open file: %@\n", filePath);
    [self dealloc];
    return nil;
  }
  
  return self;
}

- (BCSequence *)nextSequence
{
  // find beginning of FASTQ entry
  char c;
  while (!feof(fileHandle)) {
    fread(&c, sizeof(char), 1, fileHandle);
    if (feof(fileHandle)) break;
    
    if (c == '@') break;
  }
  
  // no more sequences
  if (feof(fileHandle)) return nil;
  
  // FASTQ header
  NSMutableString *fqHeader = [NSMutableString string];
  fread(&c, sizeof(char), 1, fileHandle);
  while ((c != '\n') && (!feof(fileHandle))) {
    [fqHeader appendFormat: @"%c", c];
    fread(&c, sizeof(char), 1, fileHandle);
  }
  BCAnnotation *fq = [BCAnnotation annotationWithName:BCAnnotationIdentity content:fqHeader];
  
  // sequence data
  NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
  NSMutableString *sequenceString = [NSMutableString string];
  fread(&c, sizeof(char), 1, fileHandle);
  while ((c != '+') && (!feof(fileHandle))) {
    if (![charSet characterIsMember:c]) [sequenceString appendFormat: @"%c", c];
    
    fread(&c, sizeof(char), 1, fileHandle);
  }
  
  // quality header
  while ((c != '\n') && (!feof(fileHandle))) {
    fread(&c, sizeof(char), 1, fileHandle);
  }
  
  // quality string
  NSMutableString *qualityString = [NSMutableString string];
  fread(&c, sizeof(char), 1, fileHandle);
  while ((c != '\n') && (!feof(fileHandle))) {
    if (![charSet characterIsMember:c]) [qualityString appendFormat: @"%c", c];
    
    fread(&c, sizeof(char), 1, fileHandle);
  }
  BCAnnotation *qs = [BCAnnotation annotationWithName:BCAnnotationQuality content:qualityString];

  BCSequence *seq = [BCSequence sequenceWithString: sequenceString];
  [seq addAnnotation: fq];
  [seq addAnnotation: qs];
  
  return seq;
}

@end

/*
 FASTQ format, sequence and Phred qualities
 
 Here is format information from the following web site:
 
 http://maq.sourceforge.net/fastq.shtml
 
 Example
 
 @EAS54_6_R1_2_1_413_324
 CCCTTCTTGTCTTCAGCGTTTCTCC
 +
 ;;3;;;;;;;;;;;;7;;;;;;;88
 @EAS54_6_R1_2_1_540_792
 TTGGCAGGCCAAGGCCGATGGATCA
 +
 ;;;;;;;;;;;7;;;;;-;;;3;83
 @EAS54_6_R1_2_1_443_348
 GTTGCTTCTGGCGTGGGTGGGGGGG
 +EAS54_6_R1_2_1_443_348
 ;;;;;;;;;;;9;7;;.7;393333
 
 FASTQ Format Specification
 
 Notations
 
 <fastq>, <blocks> and so on represents non-terminal symbols.
 Characters in red are regex-like operators.
 '\n' stands for the Return key.
 
 Syntax
 
 <fastq>	:=	<block>+
 <block>	:=	@<seqname>\n<seq>\n+[<seqname>]\n<qual>\n
 <seqname>	:=	[A-Za-z0-9_.:-]+
 <seq>	:=	[A-Za-z\n\.~]+
 <qual>	:=	[!-~\n]+
 
 Requirements
 
 * The <seqname> following '+' is optional, but if it appears right after '+',
 it should be identical to the <seqname> following '@'.
 
 * The length of <seq> is identical the length of <qual>. Each character in <qual> represents
 the phred quality of the corresponding nucleotide in <seq>.
 
 * If the Phred quality is $Q, which is a non-negative integer, the corresponding quality
 character can be calculated with the following Perl code:
 $q = chr(($Q<=93? $Q : 93) + 33);
 where chr() is the Perl function to convert an integer to a character based on the ASCII table.
 
 * Conversely, given a character $q, the corresponding Phred quality can be calculated with:
 $Q = ord($q) - 33;
 where ord() gives the ASCII code of a character.
 
 Solexa/Illumina Read Format
 
 The syntax of Solexa/Illumina read format is almost identical to the FASTQ format, but the
 qualities are scaled differently. Given a character $sq, the following Perl code gives the Phred quality $Q:
 
 $Q = 10 * log(1 + 10 ** (ord($sq) - 64) / 10.0)) / log(10);
 
 */
