//
//  BCParseGFF.h
//  BioCocoa
//
//  Created by Scott Christley on 2/21/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/*!
 @header
 @abstract Parse GFF (General Feature Format) file. 
 */

#import <Cocoa/Cocoa.h>


@interface BCParseGFF : NSObject {

}

/*!
 @method     parseFile:
 @abstract   Parses the given GFF file and returns an array of features.
 @discussion	This methods parses the given GFF file.  The GFF format
 is described at http://www.sanger.ac.uk/resources/software/gff/spec.html.
 Currently the header fields are ignored.
 
 Fields are: <seqname> <source> <feature> <start> <end> <score> <strand> <frame> [attributes] [comments]

 <seqname> The name of the sequence.

 <source> The source of this feature.

 <feature> The feature type name.
 
 <start>, <end> Integers. <start> must be less than or
 equal to <end>. Sequence numbering starts at 1.
 
 <score> A floating point value.
 
 <strand> One of '+', '-' or '.'. '.' should be used when
 strand is not relevant, e.g. for dinucleotide repeats.
 
 <frame> One of '0', '1', '2' or '.'.
 
 [attribute] From version 2 onwards, the attribute field must
 have an tag value structure following the syntax used within
 objects in a .ace file, flattened onto one line by semicolon
 separators. Tags must be standard identifiers
 ([A-Za-z][A-Za-z0-9_]*). Free text values must be quoted with
 double quotes. Note: all non-printing characters in such free
 text value strings (e.g. newlines, tabs, control characters, etc)
 must be explicitly represented by their C (UNIX) style
 backslash-escaped representation (e.g. newlines as '\n',
 tabs as '\t'). As in ACEDB, multiple values can follow a
 specific tag. The aim is to establish consistent use of
 particular tags, corresponding to an underlying implied ACEDB
 model if you want to think that way (but acedb is not required).

 */

- (NSArray *)parseFile: (NSString *)fileName;

@end
