//
//  BCSequenceFile.h
//  BioCocoa
//
//  Created by Scott Christley on 10/18/13.
//  Copyright (c) 2013 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/*!
 @header
 @abstract Provides sequential access to biological sequences stored in files.
 */

#import <Foundation/Foundation.h>
#import <BioCocoa/BCSequenceReader.h>

/*!
 @class      BCSequenceFile
 @abstract   Class that holds a biological sequence (DNA, protein, etc.)
 @discussion For handling files with a very large number of sequences.
 We do not want to read in all of sequence data into memory, instead this
 class provides the ability to read one sequence at a time.
 */

@interface BCSequenceFile : NSObject
{
  NSString *sequenceFile;
  FILE *fileHandle;

  int numberOfSequences;
  int currentSequenceNumber;
}

/*!
 @method		readSequenceFileUsingPath:
 @abstract  Initializes a BCSequenceFile object with sequence data in file.
 */
+ (id)readSequenceFileUsingPath:(NSString *)filePath;

/*!
 @method		readSequenceFileUsingPath:format:
 @abstract  Initializes a BCSequenceFile object with sequence data in file of specified format.
 */
+ (id)readSequenceFileUsingPath:(NSString *)filePath format:(BCFileFormat)aFormat;

/*!
 @method		initWithFileForReading:
 @abstract	Overridden by subclasses to handle specific file formats.
 */
- (id)initWithFileForReading:(NSString *)filePath;

/*!
 @method		numberOfSequences
 @abstract	Returns the total number of sequences in the file. This is an expensive operation
 because it must read the whole sequence file to determine the total.
 */
- (unsigned)numberOfSequences;

/*!
 @method		nextSequence
 @abstract	Returns the next sequence in the file and moves the file pointer to the next
 sequence in the file. The most efficient way to read sequences is by using a while loop.

 <pre>
 @textblock
      while (seq = [sequenceFile nextSequence]) {
         // do something
      }
 @/textblock
 </pre>
 */
- (BCSequence *)nextSequence;

- (void)closeFileHandle;

@end
