//
//  BCSequenceFile.m
//  BioCocoa
//
//  Created by Scott Christley on 10/18/13.
//  Copyright (c) 2013 The BioCocoa Project. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCSequenceFile.h"
#import "BCFastaFile.h"
#import "BCFASTQFile.h"

@implementation BCSequenceFile

+ (id)readSequenceFileUsingPath:(NSString *)filePath
{
  id result = nil;
  NSFileHandle *seqFile = [NSFileHandle fileHandleForReadingAtPath: filePath];
  
  if (!seqFile) {
    NSLog(@"Could not open file: %@\n", filePath);
    return nil;
  }
  
  // determine file type by reading some data
  NSData *someData = [seqFile readDataOfLength: 10000];
  NSString *entryString = [[NSString alloc] initWithData: someData encoding: NSASCIIStringEncoding];	// or NSUTF8StringEncoding ?
  [seqFile closeFile];
  
  // TODO: need to add other formats
	if ([entryString hasPrefix:@">"]) {
    result = [[BCFastaFile alloc] initWithFileForReading: filePath];
  } else if ([entryString hasPrefix:@"@"]) {
      result = [[BCFASTQFile alloc] initWithFileForReading: filePath];
	} else {
    NSLog(@"Unsupported cached sequence file type.\n");
    return nil;
  }
  
  return result;
}

+ (id)readSequenceFileUsingPath:(NSString *)filePath format:(BCFileFormat)aFormat
{
  id result = nil;
  
  switch (aFormat) {
    case BCFastaFileFormat:
      result = [[BCFastaFile alloc] initWithFileForReading: filePath];
      break;
    case BCSwissProtFileFormat:
      break;
    case BCPDBFileFormat:
      break;
    case BCNCBIFileFormat:
      break;
    case BCClustalFileFormat:
      break;
    case BCStriderFileFormat:
      break;
    case BCGCKFileFormat:
      break;
    case BCMacVectorFileFormat:
      break;
    case BCGDEFFileFormat:
      break;
    case BCPirFileFormat:
      break;
    case BCMSFFileFormat:
      break;
    case BCPhylipFileFormat:
      break;
    case BCNonaFileFormat:
      break;
    case BCHenningFileFormat:
      break;
    case BCFASTQFileFormat:
      result = [[BCFASTQFile alloc] initWithFileForReading: filePath];
      break;
  }
  
	return result;
}

- (id)initWithFileForReading:(NSString *)filePath
{
  [super init];
  
  sequenceFile = filePath;
  fileHandle = NULL;
  numberOfSequences = -1;
  currentSequenceNumber = -1;
  
  return self;
}

- (void)dealloc
{
  [self closeFileHandle];
  
  [super dealloc];
}

- (unsigned)numberOfSequences
{
  if (numberOfSequences < 0) {
    if (currentSequenceNumber > 0) numberOfSequences = currentSequenceNumber;
    else numberOfSequences = 0;
    
    while ([self nextSequence]) ++numberOfSequences;
    
    // TODO: go back to current sequence
  }
  
  return numberOfSequences;
}

- (BCSequence *)nextSequence
{
  return nil;
}

- (void)closeFileHandle
{
  if (fileHandle) fclose(fileHandle);
  fileHandle = NULL;
  currentSequenceNumber = -1;
}

@end
