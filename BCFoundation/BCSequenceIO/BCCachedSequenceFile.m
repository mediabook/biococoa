//
//  BCCachedSequenceFile.m
//  BioCocoa
//
//  Created by Scott Christley on 9/10/07.
//  Copyright (c) 2007-2010 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCCachedSequenceFile.h"
#import "BCCachedFastaFile.h"
#import "BCCachedSequence.h"

@implementation BCCachedSequenceFile

+ readCachedFileUsingPath:(NSString *)filePath
{
  id result = nil;
  NSFileHandle *seqFile = [NSFileHandle fileHandleForReadingAtPath: filePath];
  
  if (!seqFile) {
    NSLog(@"Could not open file: %@\n", filePath);
    return nil;
  }

  // determine file type by reading some data
  NSData *someData = [seqFile readDataOfLength: 10000];
  NSString *entryString = [[NSString alloc] initWithData: someData encoding: NSASCIIStringEncoding];	// or NSUTF8StringEncoding ?
  [seqFile closeFile];

  // TODO: currently only handle FASTA
	if ([entryString hasPrefix:@">"]) {
    result = [[BCCachedFastaFile alloc] initWithContentsOfFile: filePath];
	} else {
    NSLog(@"Unsupported cached sequence file type.\n");
    return nil;
  }

  return result;
}

+ readCachedFileUsingPath:(NSString *)filePath format:(BCFileFormat)aFormat
{
  id result = nil;
  
  switch (aFormat) {
    case BCFastaFileFormat:
		result = [[BCCachedFastaFile alloc] initWithContentsOfFile: filePath];
      break;
    case BCSwissProtFileFormat:
      break;
    case BCPDBFileFormat:
      break;
    case BCNCBIFileFormat:
      break;
    case BCClustalFileFormat:
      break;
    case BCStriderFileFormat:
      break;
    case BCGCKFileFormat:
      break;
    case BCMacVectorFileFormat:
      break;
    case BCGDEFFileFormat:
      break;
    case BCPirFileFormat:
      break;
    case BCMSFFileFormat:
      break;
    case BCPhylipFileFormat:
      break;
    case BCNonaFileFormat:
      break;
    case BCHenningFileFormat:
      break;
    case BCFASTQFileFormat:
      break;
  }

	return result;
}

- initWithContentsOfFile:(NSString *)filePath
{
  [super init];
  
  sequenceFile = filePath;
  fileHandle = NULL;
  sequenceInfo = [NSMutableDictionary new];
  sequenceList = [NSMutableArray new];
  currentSequenceNumber = -1;
  currentSequence = nil;

  return self;
}

- (void)dealloc
{
  if (fileHandle) fclose(fileHandle);
  if (sequenceInfo) [sequenceInfo release];
  if (sequenceList) [sequenceList release];

  [super dealloc];
}

- (NSMutableArray *)sequenceArray
{
	int i;
	NSMutableArray *anArray = [NSMutableArray array];
	for (i = 0; i < [sequenceList count]; ++i) {
		NSDictionary *d = [sequenceList objectAtIndex: i];
		BCCachedSequence *aSeq = [[BCCachedSequence alloc] initWithCachedSequenceFile: self andInfo: d];
		[aSeq autorelease];
		[anArray addObject: aSeq];
	}

	return anArray;
}

- (unsigned)numberOfSequences { return [sequenceList count]; }
- (NSDictionary *)infoForSequence:(NSString *)seqID { return [sequenceInfo objectForKey: seqID]; }
- (NSDictionary *)infoForSequenceNumber:(int)seqNum { return [sequenceList objectAtIndex: seqNum]; }

- (char)symbolAtPosition:(unsigned long long)aPos forSequence:(NSString *)seqID
{
  char c = 0;
  [self symbols: &c atPosition: aPos ofLength: 1 forSequence: seqID];

  return c;
}

- (char)symbolAtPosition:(unsigned long long)aPos forSequenceNumber:(int)seqNum
{
  char c = 0;
  [self symbols: &c atPosition: aPos ofLength: 1 forSequenceNumber: seqNum];

  return c;
}

- (int)symbols:(char *)aBuffer atPosition:(unsigned long long)aPos ofLength:(unsigned)aLen forSequenceNumber:(int)seqNum
{
  return 0;
}

- (int)symbols:(char *)aBuffer atPosition:(unsigned long long)aPos ofLength:(unsigned)aLen forSequence:(NSString *)seqID
{
  return 0;
}

- (void)closeFileHandle
{
  if (fileHandle) fclose(fileHandle);
  fileHandle = NULL;
  currentSequenceNumber = -1;
  currentSequence = nil;
}

@end
