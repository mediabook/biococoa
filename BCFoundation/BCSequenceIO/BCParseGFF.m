//
//  BCParseGFF.m
//  BioCocoa
//
//  Created by Scott Christley on 2/21/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//

#import "BCParseGFF.h"


@implementation BCParseGFF

- (NSArray *)parseFile: (NSString *)fileName
{
	NSError *e;
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	NSString *s = [NSString stringWithContentsOfFile: fileName encoding: NSUTF8StringEncoding error: &e];
	
	if (!s) {
    // try different encoding
    s = [NSString stringWithContentsOfFile: fileName encoding: NSISOLatin1StringEncoding error: &e];
	}
  
  if (!s) {
		printf("Could not open file: %s\n", [fileName UTF8String]);
		printf("%s\n", [[e localizedDescription] UTF8String]);
		return nil;
	}

  NSMutableArray *features = [NSMutableArray new];
  NSArray *a = [s componentsSeparatedByString: @"\n"];
  int i;
  for (i = 0; i < [a count]; ++i) {
    NSString *line = [a objectAtIndex: i];
    if (!line || [line length] == 0) continue;
    if ([line characterAtIndex: 0] == '#') continue;
    
    NSString *val;
    int intVal;
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    NSScanner *scanner = [NSScanner scannerWithString: [a objectAtIndex: i]];

    [scanner scanUpToCharactersFromSet: [NSCharacterSet whitespaceAndNewlineCharacterSet] intoString: &val];
    [d setObject: val forKey: @"seqname"];

    [scanner scanUpToCharactersFromSet: [NSCharacterSet whitespaceAndNewlineCharacterSet] intoString: &val];
    [d setObject: val forKey: @"source"];

    [scanner scanUpToCharactersFromSet: [NSCharacterSet whitespaceAndNewlineCharacterSet] intoString: &val];
    [d setObject: val forKey: @"feature"];

    [scanner scanInt: &intVal];
    [d setObject: [NSNumber numberWithInt: intVal] forKey: @"start"];

    [scanner scanInt: &intVal];
    [d setObject: [NSNumber numberWithInt: intVal] forKey: @"end"];

    [scanner scanUpToCharactersFromSet: [NSCharacterSet whitespaceAndNewlineCharacterSet] intoString: &val];
    if (![val isEqualToString: @"."])
      [d setObject: [NSNumber numberWithDouble: [val doubleValue]] forKey: @"score"];

    [scanner scanUpToCharactersFromSet: [NSCharacterSet whitespaceAndNewlineCharacterSet] intoString: &val];
    if (![val isEqualToString: @"."])
      [d setObject: val forKey: @"strand"];
    
    [scanner scanUpToCharactersFromSet: [NSCharacterSet whitespaceAndNewlineCharacterSet] intoString: &val];
    if (![val isEqualToString: @"."])
      [d setObject: [NSNumber numberWithInt: [val intValue]] forKey: @"frame"];

    if ([scanner scanUpToCharactersFromSet: [NSCharacterSet whitespaceAndNewlineCharacterSet] intoString: &val])
      [d setObject:val forKey:@"attribute"];

    [features addObject: d];
    //printf("%s\n", [[d description] UTF8String]);
  }
  
  [pool release];
  return features;
}

@end
